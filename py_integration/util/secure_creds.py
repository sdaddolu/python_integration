# encryption/decryption using base64 library
import base64

def encrypt_password(original_password):
    password = original_password
    encrypted_password = base64.b64encode(password.encode("utf-8"))
    return encrypted_password

def decrypt_password(encrypted_password):
    decrypted_password = base64.b64decode(encrypted_password).decode("utf-8")
    return decrypted_password


if __name__ == "__main__":
    pass
    #rabbit_paasword = encrypt_password("olJGt_IrX1Pb3FCIcJhX0LWn5K4w-CWx")
    #print("Encrypted RabbitMQ password:", rabbit_paasword)
    #dv.password = rabbit_paasword

    #db2_password = encrypt_password("pythonent1")
    #print("Encrypted DB2 password:", db2_password)
    #dv.db2_password = db2_password

