import py_integration.processOrders.db_access as dba
import py_integration.processOrders.insert_rabbitMQ as rb
from collections import defaultdict
from datetime import datetime
from py_integration.configurations.environment_properties import *
logger = logging.getLogger(__name__)


def julian_to_date(j_date):
    try:
        if j_date:
            date_obj = datetime.strptime(j_date, '%y%j').date()
            standard_date = date_obj.strftime("%m/%d/%Y")
            return standard_date
        else:
            return ""
    except Exception as e:
        print("Exception caught, error: ", e)
        logger.info("Py_Integrator: Exception caught in julian_to_date function, error: " + str(e))


class AccountUpdate(object):
    def __init__(self, call_type, ukid):
        self.ukid = ukid
        self.call_type = call_type
        self.atriumId = ""
        self.creditLimit = ""
        self.paymentInstrument = ""
        self.paymentTerms = ""
        self.dateUpdated = ""
        self.timeUpdated = ""
        self.paymentTermsDesc = ""
        self.data = None

    def process_order(self):
        try:
            preserve_dateUpdated = 0
            db_query = "select CFAN8 as atriumId, CFACL as creditLimit, CFRYIN as paymentInstrument, " \
                       "CFTRAR as paymentTerms, CFCRTJ as dateUpdated, CFCRTM as timeUpdated, PNPTD as paymentTermsDesc, " \
                       "CFCREATBY as createBy, CFAC12 as lineOfBusiness from " + db2_library + ".F5503012 inner join " + db2_library + ".F0014 " \
                        "on CFTRAR = PNPTC where CFAN8 = " + self.ukid + " and CFPTUPUSER = 'P'"
            logger.info("Py_Integrator: From process_order function, calling 'dba.ReadDataFromDB2.read_data_from_ibm_db2(db_query)' "
                        "from accountUpdate code to read records from DB2 table.")
            logger.info("Py_Integrator: Select Query is: " + db_query)
            self.data = dba.ReadDataFromDB2.read_data_from_ibm_db2(db_query)
            #print(self.data)
            if self.data:
                logger.info("Py_Integrator: able to read and got the records from db.")
            else:
                logger.error("Py_Integrator: No data received from db, Exiting...")
                exit(0)
            payload_dict = defaultdict(list)
            payload_data = {
                    "atriumId": "",
                    "creditLimit": 0,
                    "paymentInstrument": "",
                    "paymentTerms": "",
                    "dateUpdated": 0,
                    "timeUpdated": 0,
                    "lineOfBusiness": ""
            }
            l = set()
            for r in self.data:
                l.add(str(r[2]))
            l = list(l)
            for inv in l:
                for row in self.data:
                    if inv == str(row[2]):
                        payload_dict[inv].append(row)
            for k, v in payload_dict.items():
                for item in v:
                    self.atriumId = "" if not item[0] else item[0]
                    self.creditLimit = "" if not item[1] else item[1]
                    self.paymentInstrument = 0 if not item[2] else item[2]
                    self.paymentTerms = 0 if not item[3] else item[3]
                    self.dateUpdated = 0 if not item[4] else item[4]
                    self.timeUpdated = "" if not item[5] else item[5]
                    self.paymentTermsDesc = "" if not item[6] else item[6]
                    self.createBy = "" if not item[7] else item[7]
                    self.lineOfBusiness = "" if not item[8] else item[8]
                    preserve_dateUpdated = self.dateUpdated
                    self.dateUpdated = julian_to_date(str(int(self.dateUpdated))[1:])
                    paymentTermsDescSplit = self.paymentTermsDesc.strip().split()

                    payload_data["atriumId"] = int(self.atriumId)
                    payload_data["creditLimit"] = int(self.creditLimit)
                    payload_data["paymentInstrument"] = self.paymentInstrument
                    payload_data["paymentTerms"] = self.paymentTerms.strip() + " " + paymentTermsDescSplit[0] + \
                                                   paymentTermsDescSplit[1]
                    payload_data["dateUpdated"] = self.dateUpdated
                    payload_data["timeUpdated"] = int(self.timeUpdated)
                    payload_data["lineOfBusiness"] = str(self.lineOfBusiness)
                logger.debug("Py_Integrator: Writing to RabbitMQ, customerNumber: " + str(payload_data["atriumId"]) + "\n")
                rb.InsertToRabbitMQ.insert_into_rabbitmq(payload_data, self.call_type)
                if rb.InsertToRabbitMQ.msg_status == 'Y':
                    logger.debug("Py_Integrator: Successfully inserted message to RabbitMQ")
                else:
                    logger.error("Py_Integrator: Failed to insert message on to RabbitMQ")
                logger.info("Py_Integrator: Now updating message status to DB ('Y' for success 'E' for error).")
                dba.UpdateDB2.update_db2(rb.InsertToRabbitMQ.msg_status, self.call_type, preserve_dateUpdated,
                                         payload_data["timeUpdated"],self.createBy,self.atriumId)
                logger.info("Py_Integrator: Successfully updated message status to DB ('Y' for success 'E' for error).")
                payload_data = {
                    "atriumId": "",
                    "creditLimit": 0,
                    "paymentInstrument": "",
                    "paymentTerms": "",
                    "dateUpdated": 0,
                    "timeUpdated": 0,
                    "lineOfBusiness": ""
                }
        except Exception as e:
            print("Exception caught, error message: ", e)
            logger.debug("Py_Integrator: Exiting...Caught an exception, error: " + str(e))
            exit(0)
