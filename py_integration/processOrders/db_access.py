import py_integration.configurations.environment_properties as prop
from py_integration.connections.db_connection import *
from datetime import datetime
from datetime import date
logger = prop.logging.getLogger(__name__)

class ReadDataFromDB2(object):
    cursor = None
    call_type = None

    def __init__(self):
        pass
        #print("Reading records from IBM DB2.")

    @classmethod
    def read_data_from_ibm_db2(cls, db_query):
        conn = DB2Connection.get_db2_connection()
        cursor = conn.cursor()
        cursor.execute(db_query)
        rows = cursor.fetchall()
        #print(rows)
        return rows


class UpdateDB2(object):
    cursor = None

    @classmethod
    def pydate_to_julian_date (cls, std_date):
        return "1" + str(std_date.strftime('%y')) + std_date.strftime('%j')

    @classmethod
    def update_db2(cls, status, call_type, payload_data, sku, nextStatus, ukid):
        now = datetime.now()
        now.strftime("%H:%M:%S")
        currentTime = int("".join(now.strftime("%H:%M:%S").split(":")))
        currentDate = date.today()
        currentDate = cls.pydate_to_julian_date(currentDate)
        #currentDate = currentDate.strftime("%m/%d/%Y")

        if call_type == "INVOICE":
            update_query = {
                "Y": "update " + prop.db2_library + ".F5542008 set m2#eo = 'Y', m2upmj = " + currentDate + ", "
                     "m2tday = " + str(currentTime) + ", m2pid = '" + call_type + "', m2user = '" + prop.m2user + "'"
                     " where m2doc = '" + str(payload_data) + "' and m2nxtr = " + nextStatus + " and m2ukid = " + ukid,
                "E": "update " + prop.db2_library + ".F5542008 set m2#eo = 'E', m2upmj = " + currentDate + ", "
                     "m2tday = " + str(currentTime) + ", m2pid = '" + call_type + "', m2user = '" + prop.m2user + "' "
                     "where m2doc = '" + str(payload_data) + "' and m2nxtr = " + str(nextStatus) + " and m2ukid = " + ukid
                }
        elif call_type == "SHIPMENT":
            update_query = {
                "Y": "update " + prop.db2_library + ".F5542008 set m2#eo = 'Y', m2upmj = " + currentDate + ", "
                     "m2tday = " + str(currentTime) + ", m2pid = '" + call_type + "', m2user = '" + prop.m2user + "'"
                     " where m2doco = '" + str(payload_data) + "' and m2nxtr = " + nextStatus + " and m2ukid = " + ukid,
                "E": "update " + prop.db2_library + ".F5542008 set m2#eo = 'E', m2upmj = " + currentDate + ", "
                     "m2tday = " + str(currentTime) + ", m2pid = '" + call_type + "', m2user = '" + prop.m2user + "' "
                     "where m2doco = '" + str(payload_data) + "' and m2nxtr = " + str(nextStatus) + " and m2ukid = " + ukid
                }
        elif call_type == "ACCOUNTUP":
            update_query = {
                "Y": "update " + prop.db2_library + ".F5503012 set CFPTUPUSER = 'Y', CFUPMJ = " + currentDate + ", "
                        "CFCFRUPMT = " + str(currentTime) + " where CFAN8 = " + str(ukid) + " and CFCRTJ = "
                        + str(payload_data) + " and CFCRTM = " + str(sku) + " and CFCREATBY = '" + str(nextStatus) + "'",
                "E": "update " + prop.db2_library + ".F5503012 set CFPTUPUSER = 'E', CFUPMJ = " + currentDate + ", "
                        "CFCFRUPMT = " + str(currentTime) + " where CFAN8 = " + str(ukid) + " and CFCRTJ = "
                        + str(payload_data) + " and CFCRTM = " + str(sku) + " and CFCREATBY = '" + str(nextStatus) + "'"
                }
        print(update_query)
        cls.cursor = DB2Connection.db2_conn.cursor()
        cls.cursor.execute(update_query.get(status))
        cls.cursor.commit()
        logger.debug("Py_Integrator: Updated the message status successfully into DB2 table.")
