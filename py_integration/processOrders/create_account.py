import pika
import json
from py_integration.connections.rabbitMQ_connection import RabbitMQConnection
from py_integration.configurations.environment_properties import *
from py_integration.connections.db_connection import *
from datetime import datetime
from datetime import date
logger = logging.getLogger(__name__)


class AccountsCreate:
    def __init__(self, call_type, ukid):
        self.call_type = call_type
        self.ukid = ukid
        self.atriumId = 0
        self.mailingName = ""
        self.accountName = ""
        self.address1 = ""
        self.address2 = ""
        self.address3 = ""
        self.city = ""
        self.country = ""
        self.poBox = ""
        self.postalCode = ""
        self.state = ""
        self.prefix = ""
        self.firstName = ""
        self.lastName = ""
        self.suffix = ""
        self.defaultBilling = ""
        self.defaultShipping = ""
        self.parentNumber = ""
        self.affiliateParent = ""
        self.businessUnit = ""
        self.customerGrouping = ""
        self.customerPriceGroup = ""
        self.fax = ""
        self.invoicePreference = ""
        self.language = ""
        self.lineOfBusiness = ""
        self.phone = ""
        self.practitionerType = ""
        self.searchType = ""
        self.taxExemptCertificate = ""
        self.transactionAction = ""
        self.addressDeliveryInstructions = ""
        self.addressDeliveryInstructions2 = ""
        self.applyFreightRules = ""
        self.freightHandling = ""
        self.carrier = ""
        self.collectionManager = ""
        self.creditLimit = 0
        self.creditManager = ""
        self.currencyCode = ""
        self.customerInactive = ""
        self.dateLastPaid = ""
        self.paymentInstrument = ""
        self.paymentTerms = ""
        self.taxExplanation = ""
        self.zone = ""
        self.email = ""
        self.rep = 0
        self.iRep = 0
        self.code = ""
        self.value = ""

        self.payload = {
                            "atriumId": 2125370, 			 		                    #number SAAN8
		                    "mailingName": "NEW CITY Hospital",                     #string SAMLNM
			                "accountName": "NEW CITY Hospital",                     #string SAALPH
			                "address": {					 		                    #customer address
							                "address1": "Dr.Patil Smita",  		    #string SAADD1
							                "address2": "59694 Calgary Loop # 22517",  	#string SAADD2
							                "address3": "59694 Calgary Loop # 22517",  	#string SAADD3
							                "city": "Bend",      		                #string SACTY1
                                            "country": "US",   		                    #string SACTR
                                            "poBox": "",     		                    #string*
                                            "postalCode": "97702-9218", 		        #string SAADDZ
                                            "state": "OR", 			                    #string SAADDS
                                            "prefix": "",			                    #string** SA
                            },
                            "firstName": "A2C ANTI AGING",      						#string **
                            "lastName": "CLINIC",				            			#string **
                            "suffix": "",                                               #string **
                            "defaultBilling": True,                                     #boolean
                            "defaultShipping": "",                                      #boolean?**
                            "parentNumber": "",                                         #string*
                            "affiliateParent": "",                                      #string* SAALKY
                            "businessUnit": "2011095",                                  #string SAMCU
                            "customerGrouping": "ATP",                                  #string SAAC28
                            "customerPriceGroup": "105",                                #string SAAC30
                            "fax": "12345",                                                  #string* SAPH2
                            "invoicePreference": "MNI",                                 #string SAAC27
                            "language": "",                                             #string* SALNGP
                            "lineOfBusiness": "USP",                                    #string SAAC12
                            "phone": "9807373598",                                      #string SAPH1
                            "practitionerType": "10",                                   #string? SAAC19
                            "searchType": "C",                                          #string SAAT1
                            "taxExemptCertificate": "",                                 #string SATXCT
                            "transactionAction": "A",                                   #character- 'A' for CREATE and 'C' for UPDATE SATNAC
                            "addressDeliveryInstructions": "Test1",                          #string* SADEL1
                            "addressDeliveryInstructions2": "Test2",                         #string* SADEL2
                            "applyFreightRules": "A",                                   #string SASTOP
                            "freightHandling": "A",                                    #string SAFRTH
                            "carrier": 30957,                                         #mentioned number but came as string SACARS
                            "collectionManager": "CM01",                                #number SACLMG
                            "creditLimit": 2500.0,                                    #number SAACL
                            "creditManager": "CR01",                                    #number* SACMGR
                            "currencyCode": "USD",                                      #string SACRCD
                            "customerInactive": "",                                     #dont know what this for ???* SACUSTS
                            "dateLastPaid": "06/10/2019",                               #regular date format MM/DD/YYYY SADLP
                            "paymentInstrument": ">",                                   #string SARYIN
                            "paymentTerms": "002",                                      #string SATRAR
                            "taxExplanation": "",                                       #string* SAEXR1
                            "zone": "",                                                 #string* SAZON
                            "email": "patil@gmail.com",                                  #string SAEMAL
                            "rep": 99998,                                             #number SACMC1
                            "iRep": 88888,                                            #number SACMC2
                            "customAttributes": [{
                                                    "code": "",                         #dont know what this for ???*
                                                    "value": "",                        #dont know what this for ???*
                            }]
        }

    def process_special_char(self, s):
        if not s:
            return s
        new_str = ''
        i = 0
        s = str(s)
        while i < len(s):
            if s[i] == "'":
                new_str += "\'\'"
                i += 1
                continue
            new_str += s[i]
            i += 1
        s = new_str
        return s

    def pydate_to_julian_date (self, std_date):
        try:
            if not std_date:
                return ""
            else:
                return "1" + str(std_date.strftime('%y')) + std_date.strftime('%j')
        except Exception as e:
            print("Exception caught, error: ", e)
            logger.info("Py_Integrator: Exception caught in pydate_to_julian_date function, error: " + str(e))

    def get_account_record_message(self, channel, method_frame, header_frame, message_body):
        try:
            logger.info("Py_Integrator: Message received in get_account_record_message from account queue.")
            message = message_body
            message = json.loads(message.decode("utf8"))
            channel.basic_ack(delivery_tag=method_frame.delivery_tag)
            logger.info(str(message))
            if self.is_account_create_set_on() == 'Y':
                try:
                    self.insert_into_db2_accounts_staging_table(message_body)
                    logger.info("Py_Integrator: Successfully inserted message into DB2 table F5501002.")
                except Exception as e:
                    self.insert_to_DL_queue(message)
                    logger.error("Py_Integrator: Unable to write account record to DB2 table F5501002, please investigate. Error: " + str(e))
            else:
                logger.error("Py_Integrator: Account create is set off, please make sure it is set on to create account.")
                self.insert_to_DL_queue(message)
                exit(0)
        except Exception as e:
            print("Exception caught, error: ", e)
            logger.info("Py_Integrator: Exception caught in get_account_record_message function, error: " + str(e))

    def insert_to_DL_queue(self, payload):
        try:
            account_exchange = rabbitmq_account_exchange
            account_dl_routing_key = rabbitmq_account_dl_routing_key
            payload_json = json.dumps(payload, indent=4)
            #print(payload_json)
            channel = RabbitMQConnection.get_connection()
            channel.basic_publish(exchange=account_exchange, routing_key=account_dl_routing_key,
                                  body=payload_json, properties=pika.BasicProperties(content_type="application/json",
                                  delivery_mode=2))
            logger.error("Py_Integrator: Msg successfully inserted to account DL Queue.")
            print("Msg successfully inserted to DL Queue.")
        except Exception as e:
            logger.error("Py_Integrator: Message not inserted successfully into RabbitMQ DL Queue. Error: " + str(e))

    def insert_into_db2_accounts_staging_table(self, message_record):
        try:
            
            message_record = json.loads(message_record.decode("utf8"))
            self.atriumId = message_record.get("atriumId", str(0))
            self.mailingName = self.process_special_char(message_record.get("mailingName", ""))
            self.accountName = self.process_special_char(message_record.get("accountName", ""))
            self.address1 = self.process_special_char(message_record.get("address").get("address1", ""))
            self.address2 = self.process_special_char(message_record.get("address").get("address2", ""))
            self.address3 = self.process_special_char(message_record.get("address").get("address3", ""))
            self.city = self.process_special_char(message_record.get("address").get("city", ""))
            self.country = message_record.get("address").get("country", "")
            self.poBox = message_record.get("address").get("poBox", "")
            self.postalCode = message_record.get("address").get("postalCode", "")
            self.state = message_record.get("address").get("state", "")
            self.prefix = message_record.get("address").get("prefix", "")
            self.firstName = self.process_special_char(message_record.get("firstName", ""))
            self.lastName = self.process_special_char(message_record.get("lastName", ""))
            self.suffix = self.process_special_char(message_record.get("suffix", ""))
            self.defaultBilling = self.process_special_char(str(message_record.get("defaultBilling", "")))
            self.defaultShipping = self.process_special_char(message_record.get("defaultShipping", ""))
            self.parentNumber = message_record.get("parentNumber", str(0))
            self.affiliateParent = message_record.get("affiliateParent", "")
            self.businessUnit = '{0: >12}'.format(message_record.get("businessUnit", ""))
            self.customerGrouping = message_record.get("customerGrouping", "")
            self.customerPriceGroup = message_record.get("customerPriceGroup", "")
            self.fax = message_record.get("fax", "")
            self.invoicePreference = message_record.get("invoicePreference", "")
            self.language = message_record.get("language", "")
            self.lineOfBusiness = message_record.get("lineOfBusiness", "")
            self.phone = message_record.get("phone", "")
            self.practitionerType = message_record.get("practitionerType", "")
            self.searchType = message_record.get("searchType", "")
            self.taxExemptCertificate = message_record.get("taxExemptCertificate", "")
            self.transactionAction = message_record.get("transactionAction", "")
            self.addressDeliveryInstructions = self.process_special_char(message_record.get("addressDeliveryInstructions", ""))
            self.addressDeliveryInstructions2 = self.process_special_char(message_record.get("addressDeliveryInstructions2", ""))
            self.applyFreightRules = message_record.get("applyFreightRules", "")
            self.freightHandling = message_record.get("freightHandling", "")
            self.carrier = str(message_record.get("carrier", str(0)))
            self.collectionManager = str(message_record.get("collectionManager", ""))
            self.creditLimit = str(message_record.get("creditLimit", str(0)))
            self.creditManager = str(message_record.get("creditManager", ""))
            self.currencyCode = message_record.get("currencyCode", "")
            self.customerInactive = message_record.get("customerInactive", "")
            if message_record.get("dateLastPaid"):
                last_paid_date_obj = datetime.strptime(message_record.get("dateLastPaid"), '%m/%d/%Y')
                self.dateLastPaid = self.pydate_to_julian_date(last_paid_date_obj.date())
            else:
                self.dateLastPaid = str(0)
            self.paymentInstrument = message_record.get("paymentInstrument", "")
            self.paymentTerms = self.process_special_char(message_record.get("paymentTerms", ""))
            self.taxExplanation = self.process_special_char(message_record.get("taxExplanation", ""))
            self.zone = message_record.get("zone", "")
            self.email = self.process_special_char(message_record.get("email", ""))
            self.rep = str(message_record.get("rep", str(0)))
            self.iRep = str(message_record.get("iRep", str(0)))
            self.code = message_record.get("customAttributes")[0].get("code", "") if message_record.get("customAttributes") else ""
            self.value = message_record.get("customAttributes")[0].get("value", "") if message_record.get("customAttributes") else ""

            now = datetime.now()
            now.strftime("%H:%M:%S")
            currentTime = int("".join(now.strftime("%H:%M:%S").split(":")))
            currentDate = date.today()
            current_jl_date_Date = self.pydate_to_julian_date(currentDate)
            insert_query = "insert into " + db2_library + ".F5501002 (SAAN8, SATRDJ, SATRTM, SAEDSP, SATNAC, SAMLNM," \
                                                          " SAALPH, SAALKY, SAAT1, SAADD1, SAADD2, SAADD3, SACTY1," \
                                                          " SAADDS, SAADDZ, SACTR, SAMCU, SAPH1, SAPH2, SAAC12," \
                                                          " SAAC19, SAAC27, SAAC28, SAAC30, SALNGP, SATXCT, SAACL," \
                                                          " SASTOP, SACARS, SACLMG, SACMGR, SACRCD, SACUSTS, SADEL1," \
                                                          " SADEL2, SADLP, SAEXR1, SAFRTH, SARYIN, SATRAR, SAZON," \
                                                          " SAEMAL, SACMC1, SACMC2, SAFLAG, SA55ERMSG, SAURRF," \
                                                          " SAURAB, SAURDT, SAURCD, SAUSER, SAUPMJ, SATDAY, SAPID," \
                                                          " SAJOBN) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," \
                                                          " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," \
                                                          " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," \
                                                          " ?, ?, ?)"
            insert_query_values = (self.atriumId, int(current_jl_date_Date), currentTime, 'N', self.transactionAction,
                                   self.mailingName , self.accountName, self.affiliateParent, self.searchType,
                                   self.address1, self.address2, self.address3, self.city, self.state, self.postalCode,
                                   self.country, self.businessUnit, self.phone, self.fax, self.lineOfBusiness,
                                   self.practitionerType, self.invoicePreference, self.customerGrouping,
                                   self.customerPriceGroup, self.language, self.taxExemptCertificate,
                                   int(self.creditLimit), self.applyFreightRules, self.carrier, self.collectionManager,
                                   self.creditManager, self.currencyCode, self.customerInactive,
                                   self.addressDeliveryInstructions, self.addressDeliveryInstructions2,
                                   self.dateLastPaid, self.taxExplanation, self.freightHandling, self.paymentInstrument,
                                   self.paymentTerms, self.zone, self.email, self.rep, self.iRep, '', '', '', 0, 0, '',
                                   m2user, int(current_jl_date_Date), currentTime, program_id, '0')

            logger.info("Py_Integrator: Insert Query -> " + insert_query + " and insert values are : " )
            logger.info(insert_query_values)
            conn = DB2Connection.get_db2_connection()
            cursor = conn.cursor()
            cursor.execute(insert_query, insert_query_values)
            cursor.commit()
            logger.info("Py_Integrator: Insert into table F5501002 is successful.")
        except Exception as e:
            print("Exception caught, error message: ", e)
            logger.debug("Py_Integrator: Exiting...Caught an exception, error: " + str(e))
            #exit(0)

    def is_account_create_set_on(self):
        try:
            acc_set_query = "select COFLAG from " + db2_library + ".F5642CON where CO55CTYPE='ACCOUNTADD'"
            #print(acc_set_query)
            conn = DB2Connection.get_db2_connection()
            cursor = conn.cursor()
            cursor.execute(acc_set_query)
            row = cursor.fetchone()
            if row:
                return row[0]
            else:
                raise Exception
        except Exception as e:
            print("Exception caught, error message: ", e)
            logger.debug("Py_Integrator: Exiting...Caught an exception in is_account_create_set_on, either no "
                         "record found or the row is empty. error: " + str(e))
            #exit(0)

    def process_order(self):
        while(True):
            try:
                credentials = pika.PlainCredentials(rabbitmq_username, decrypt_password(rabbitmq_password))
                parameters = pika.ConnectionParameters(rabbitmq_host, rabbitmq_port, rabbitmq_vhost, credentials)
                connection = pika.BlockingConnection(parameters)
                channel = connection.channel()
                channel.basic_consume(rabbitmq_account_queue, self.get_account_record_message)
                try:
                    channel.start_consuming()
                except KeyboardInterrupt:
                    channel.stop_consuming()
                    logger.error("Py_Integrator: Consuming interrupted.")
                connection.close()
                break
            except Exception as e:
                logger.info("Py_Integrator: Caught an exception in rabbitmq consumer. Retrying" + str(e))
                continue
            except pika.exceptions.AMQPConnectionError:
                print("Py_Integrator: Connection was closed, retrying...")
                continue
