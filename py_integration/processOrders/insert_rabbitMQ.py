import py_integration.configurations.environment_properties as prop
from py_integration.connections.rabbitMQ_connection import *
logger = prop.logging.getLogger(__name__)


class InsertToRabbitMQ(object):
    channel = None
    invoice_exchange = prop.rabbitmq_invoice_exchange
    invoice_routing_key = prop.rabbitmq_invoice_routing_key
    shipment_exchange = prop.rabbitmq_shipment_exchange
    shipment_routing_key = prop.rabbitmq_shipment_routing_key
    payload_json = None
    msg_status = None

    def __init__(self, call_type):
        self.call_type = call_type

    @classmethod
    def insert_into_rabbitmq(cls, payload, nextStatus):
        #if nextStatus == ("590" or "578"):
        try:
            cls.payload_json = json.dumps(payload, indent=4)
            #print(cls.payload_json)
            cls.channel = RabbitMQConnection.get_connection()
            if nextStatus == ("590"):
                logger.debug("Py_integrator: INVOICE CALL nextStatus is : " + nextStatus)
                cls.channel.basic_publish(exchange=cls.invoice_exchange, routing_key=cls.invoice_routing_key,
                                          body=cls.payload_json,
                                          properties=pika.BasicProperties(content_type="application/json",
                                                                          delivery_mode=2))
            elif nextStatus == ("578"):
                logger.debug("Py_integrator: SHIPMENT CALL nextStatus is : " + nextStatus)
                cls.channel.basic_publish(exchange=cls.shipment_exchange, routing_key=cls.shipment_routing_key,
                                          body=cls.payload_json,
                                          properties=pika.BasicProperties(content_type="application/json",
                                                                          delivery_mode=2))
            elif nextStatus == ("ACCOUNTUP"):
                logger.debug("Py_integrator: ACCOUNTUP CALL nextStatus is : " + nextStatus)
                #cls.channel.queue_declare(queue=rabbitmq_accountUpdate_queue)
                cls.channel.basic_publish(exchange=prop.rabbitmq_accountUpdate_exchange,
                                          routing_key=prop.rabbitmq_accountUpdate_routing_key, body=cls.payload_json,
                                          properties=pika.BasicProperties(content_type="application/json",
                                                                          delivery_mode=2))
            else:
                logger.error("Py_Integrator: Order not handled for RabbitMQ.")
                cls.msg_status = "E"
            cls.msg_status = "Y"
        except Exception as e:
            cls.msg_status = "E"
            logger.error("Py_Integrator: Message not inserted successfully into RabbitMQ with error: " + str(e))
