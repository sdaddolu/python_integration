import json
import pika
from collections import defaultdict
import py_integration.processOrders.db_access as dba
from py_integration.configurations.environment_properties import *
import py_integration.configurations.environment_properties as prop
from py_integration.connections.rabbitMQ_connection import RabbitMQConnection
from py_integration.util.secure_creds import *
from py_integration.connections.db_connection import DB2Connection
from py_integration.processOrders.create_tax import CreateTax
from datetime import datetime
from datetime import date

logger = logging.getLogger(__name__)

class AsyncTax(object):
    def __init__(self, call_type, ukid):
        self.conn = None
        self.cursor = None
        self.ukid = ukid
        self.call_type = call_type
        self.type = ""
        self.companyCode = ""
        self.code = ""
        self.date = ""
        self.customerCode = ""
        self.purchaseOrderNo = ""
        self.shipFrom_line1 = ""
        self.shipFrom_line2 = ""
        self.shipFrom_line3 = ""
        self.shipFrom_city = ""
        self.shipFrom_region = ""
        self.shipFrom_country = ""
        self.shipFrom_postalCode = ""
        self.shipTo_line1 = ""
        self.shipTo_line2 = ""
        self.shipTo_line3 = ""
        self.shipTo_city = ""
        self.shipTo_region = ""
        self.shipTo_country = ""
        self.shipTo_postalCode = ""
        self.number = ""
        self.quantity = 0
        self.amount = ""
        self.taxCode = ""
        self.itemCode = ""
        self.lines_description = ""
        self.taxIncluded = ""
        self.commit = ""
        self.currencyCode = ""
        self.description = ""
        self.DocumentOrderInvoice = 0
        self.OrderType = ""
        self.CompanyKeyOrderNo = ""
        self.LineNumber = ""
        self.AddressNumber = ""
        self.AddressNumberShipTo = ""
        self.UniqueKeyIDInternal = ""
        self.UserId = ""
        self.ProgramId = ""
        self.UnitsTransactionQty = ""
        self.AmtPricePerUnit2 = ""
        self.TaxArea1 = ""
        self.Identifier2ndItem = ""
        self.DescriptionLine1 = ""
        self.DescriptionLine2 = ""
        self.DateUpdated = ""
        self.TimeOfDay = ""
        self.WorkStationId = ""
        self.EdiSuccessfullyProcess = ""
        self.AmountExtendedPrice = ""
        self.AddressLine19 = ""
        self.AddressLine29 = ""
        self.AddressLine39 = ""
        self.shipFromState = ""
        self.PhoneNumber1 = ""
        self.AddressLine1 = ""
        self.AddressLine2 = ""
        self.AddressLine3 = ""
        self.shipToState = ""
        self.PhoneNumber = ""
        self.CallType = ""
        self.OriginalPoSoNumber = 0
        self.OriginalOrderType = ""
        self.CompanyKeyOriginal = ""
        self.ActualStartDate = ""
        self.ActualStartTime = ""
        self.ActualEndDate = ""
        self.ActualEndTime = ""
        self.cur_date_time = ""
        self.orderCompany = ""
        self.totalAmount = 0
        self.totalTaxable = 0
        self.totalExempt = 0
        self.totalTax = 0
        self.id = 0
        self.code = 0
        self.companyId = ""
        self.date = ""
        self.status = ""
        self.type = ""
        self.orderType = ""
        self.orderId = 0
        self.lineId = 0
        self.atriumId = 0
        self.lineAmount = 0
        self.exemptAmount = 0
        self.taxableAmount = 0
        self.tax = 0
        self.sku = ""
        self.taxAuthority1 = ""
        self.taxRate1 = 0
        self.taxAuthority2 = 0
        self.taxRate2 = 0
        self.taxAuthority3 = 0
        self.taxRate3 = 0
        self.taxAuthority4 = 0
        self.taxAuthority5 = 0
        self.taxRate5 = 0
        self.taxAmount1 = 0
        self.taxAmount2 = 0
        self.taxAmount3 = 0
        self.taxAmount4 = 0
        self.taxAmount5 = 0
        self.cur_time = 0
        self.invoiceId = 0
        self.invoiceType = ""
        self.invoiceCompany = ""
        self.odev01 = ""
        self.itemCode_msg = ""
        self.payload_json = ""
        self.DateUpdated_julian = 0
        self.tax_obj = CreateTax(self.call_type, self.ukid)

    def get_tax_data(self):
        try:
            self.conn = DB2Connection.get_db2_connection()
            self.cursor = self.conn.cursor()
            db_query =  "select RDDOCO as DocumentOrderInvoice, RDDCTO as OrderType, RDKCOO as CompanyKeyOrderNo, " \
            "RDLNID as LineNumber, RDAN8 as AddressNumber, RDSHAN as AddressNumberShipTo, RDUKID as UniqueKeyIDInternal," \
            "RDUSER as UserId, RDPID as ProgramId, RDUORG as UnitsTransactionQty, RDUPRC as AmtPricePerUnit2, RDTXA1 as TaxArea1, " \
            "RDLITM as Identifier2ndItem, RDDSC1 as DescriptionLine1, RDDSC2 as DescriptionLine2, RDUPMJ as DateUpdated, " \
            "RDTDAY as TimeOfDay, RDJOBN as WorkStaLNIDtionId, RDEDSP as EdiSuccessfullyProcess, RDAEXP as AmountExtendedPrice, " \
            "RDADDRLIN1 as AddressLine19, RDADDRLIN2 as AddressLine29, RDADDRLIN3 as AddressLine39, RD55ADDZ as ZipCodePostal, " \
            "RD55CTY1 as City, RD55ADDS as State, RD55CTR as Country, RDPH2 as PhoneNumber1, RDADD1 as AddressLine1, RDADD2 as AddressLine2, " \
            "RDADD3 as AddressLine3, RDADDZ as ZipCodePostal, RDCTY1 as City, RDADDS as State, RDCTR as Country, RDPH1 as PhoneNumber, " \
            "RD55CTYPE as CallType, RDOORN as OriginalPoSoNumber, RDOCTO as OriginalOrderType, RDOKCO as CompanyKeyOriginal, " \
            "RDWASJDT as ActualStartDate, RDWWAST as ActualStartTime, RDWAEJDT as ActualEndDate, RDWWAET as ActualEndTime " \
            "from " + prop.db2_library + ".F59AT006 where RDUKID = " + str(self.ukid) + " and RD55CTYPE = '" + self.call_type + "' " \
            "order by RDDOCO, RDKCOO, RDDCTO"
            logger.info("Py_Integrator: Select query for F59AT006 table to create avalara json request is: " + str(db_query))
            logger.info("Py_Integrator: Calling 'dba.ReadDataFromDB2.read_data_from_ibm_db2(db_query)' from create tax to read records from DB2 table F59AT006.")
            self.data = dba.ReadDataFromDB2.read_data_from_ibm_db2(db_query)
            if self.data:
                logger.info("Py_Integrator: Received data records from table F59AT006: " + str(self.data))
            else:
                logger.error("Py_Integrator: No data received from db, Exiting...")
            self.taxIncluded = 'true' if self.call_type == 'TAXGETAR' else 'false'
            payload_dict = defaultdict(list)
            l = set()
            for r in self.data:
                l.add(str(r[0]))
            l = list(l)
            for inv in l:
                for row in self.data:
                    if inv == str(row[0]):
                        payload_dict[inv].append(row)
            for k, v in payload_dict.items():
                for item in v:
                    self.tax_obj.orderId = str(0) if not item[0] else item[0]
                    self.tax_obj.orderType = "" if not item[1] else item[1]
                    self.tax_obj.CompanyKeyOrderNo = str(0) if not item[2] else str(item[2])
                    self.tax_obj.LineNumber = str(0) if not item[3] else item[3]
                    self.tax_obj.AddressNumber = str(0) if not item[4] else item[4]
                    self.tax_obj.AddressNumberShipTo = str(0) if not item[5] else item[5]
                    self.tax_obj.UniqueKeyIDInternal = str(0) if not item[6] else item[6]
                    self.tax_obj.UserId = "" if not item[7] else item[7]
                    self.tax_obj.ProgramId = "" if not item[8] else item[8]
                    self.tax_obj.UnitsTransactionQty = str(0) if not item[9] else item[9]
                    self.tax_obj.AmtPricePerUnit2 = str(0) if not item[10] else item[10]
                    self.tax_obj.TaxArea1 = "" if not item[11] else item[11]
                    self.tax_obj.Identifier2ndItem = "" if not item[12] else item[12]
                    self.tax_obj.DescriptionLine1 = "" if not item[13] else item[13]
                    self.tax_obj.DescriptionLine2 = "" if not item[14] else item[14]
                    self.tax_obj.DateUpdated_julian = str(0) if not item[15] else int(item[15])
                    self.tax_obj.DateUpdated = item[15]
                    self.tax_obj.TimeOfDay = str(0) if not item[16] else item[16]
                    self.tax_obj.WorkStationId = "" if not item[17] else item[17]
                    self.tax_obj.EdiSuccessfullyProcess = str(0) if not item[18] else item[18]
                    self.tax_obj.AmountExtendedPrice = str(0) if not item[19] else item[19]
                    self.tax_obj.AddressLine19 = str(0) if not item[20] else item[20]
                    self.tax_obj.AddressLine29 = "" if not item[21] else item[21]
                    self.tax_obj.AddressLine39 = "" if not item[22] else item[22]
                    self.tax_obj.shipFrom_postalCode = str(0) if not item[23] else item[23]
                    self.tax_obj.shipFrom_city = "" if not item[24] else item[24]
                    self.tax_obj.shipFromState = "" if not item[25] else item[25]
                    self.tax_obj.shipFrom_country = "" if not item[26] else item[26]
                    self.tax_obj.PhoneNumber1 = str(0) if not item[27] else item[27]
                    self.tax_obj.AddressLine1 = "" if not item[28] else item[28]
                    self.tax_obj.AddressLine2 = "" if not item[29] else item[29]
                    self.tax_obj.AddressLine3 = "" if not item[30] else item[30]
                    self.tax_obj.shipTo_postalCode = "" if not item[31] else item[31]
                    self.tax_obj.shipTo_city = "" if not item[32] else item[32]
                    self.tax_obj.shipToState = "" if not item[33] else item[33]
                    self.tax_obj.shipTo_country = "" if not item[34] else item[34]
                    self.tax_obj.PhoneNumber = str(0) if not item[35] else item[35]
                    self.tax_obj.CallType = "" if not item[36] else item[36]
                    self.tax_obj.OriginalPoSoNumber = str(0) if not str(item[37]).strip() else item[37]
                    self.tax_obj.OriginalOrderType = "" if not item[38] else item[38]
                    self.tax_obj.CompanyKeyOriginal = "" if not item[39] else item[39]
                    self.tax_obj.ActualStartDate = str(0)
                    self.tax_obj.ActualStartTime = str(0) if not item[41] else item[41]
                    self.tax_obj.ActualEndDate = str(0)
                    self.tax_obj.ActualEndTime = str(0) if not item[43] else item[43]
        except Exception as e:
            logger.info("Py_Integrator: Caught an exception in get_tax_data, error: " + str(e))

    def get_tax_record_message(self, channel, method_frame, header_frame, message_body):
        try:
            #self.get_tax_data()
            logger.info("Py_Integrator: Received order record message:")
            logger.info("Py_Integrator: Order message: " + str(message_body) + "\nMessage header frame: " +
                        str(header_frame) + "\nmessage delivery_tag: " + str(method_frame.delivery_tag))
            message = message_body
            message = json.loads(message.decode("utf8"))
            logger.info("Py_Integrator: After converting message to json: " + str(message))
            channel.basic_ack(delivery_tag=method_frame.delivery_tag)
            status_flag = self.is_tax_create_set_on()
            if status_flag == 'Y':
                try:
                    logger.info("Py_Integrator: Calling insert_avalara_tax_info_to_db2_tables to insert message record to insert into DB2 staging table.")
                    logger.info("Py_Integrator: call_type" + self.call_type)
                    self.tax_obj.insert_avalara_tax_info_to_db2_tables(message_body)
                    logger.info("Py_Integrator: Successfully inserted avalara message into DB2 table.")
                except Exception as e:
                    logger.info("Py_Integrator: Caught an exception while writing message to db2 tables, writing message to Tax DL queue.")
                    self.insert_to_tax_DL_queue(message)
            else:
                self.insert_to_tax_DL_queue(message)
                logger.info("Py_Integrator: The Tax message went to Tax DL queue. The status flag in the config table for "
                             "Tax is not set to 'Y', status flag is: " + str(status_flag))
        except Exception as e:
            logger.info("Py_Integrator: Exiting...Caught an exception while processing message in get_tax_record_message, error: " + str(e))
            #exit(1)

    def insert_to_tax_DL_queue(self, payload):
        try:
            logger.info("Py_Integrator: Inserting message to DL Queue.")
            payload_json = json.dumps(payload, indent=4)
            logger.info("Py_Integrator: Payload json is: " + str(payload_json))
            channel = RabbitMQConnection.get_connection()
            channel.basic_publish(exchange=rabbitmq_tax_DL_exchange, routing_key=rabbitmq_tax_DL_routing_key,
                                  body=payload_json, properties=pika.BasicProperties(content_type="application/json",
                                  delivery_mode=2))
            logger.info("Py_Integrator: Msg successfully inserted to Tax DL Queue.")
        except Exception as e:
            logger.info("Py_Integrator: Message not inserted successfully into RabbitMQ Tax DL Queue. Error: " + str(e))

    def is_tax_create_set_on(self):
        try:
            logger.info("Py_Integrator: Inside is_tax_create_set_on function")
            tax_set_query = "select COFLAG from " + prop.db2_library + ".F5642CON where CO55CTYPE='TAXES'"
            conn = DB2Connection.get_db2_connection()
            cursor = conn.cursor()
            logger.info("Py_Integrator: Executing the query: " + str(tax_set_query))
            cursor.execute(tax_set_query)
            rows = cursor.fetchone()
            return rows[0]
        except Exception as e:
            logger.info("Py_Integrator: Caught an exception in async tax in is_tax_create_set_on(), error: " + str(e))

    def process_order(self):
        while(True):
            try:
                logger.info("Py_Integrator: Waiting for messages to receive...")
                credentials = pika.PlainCredentials(rabbitmq_username, decrypt_password(rabbitmq_password))
                parameters = pika.ConnectionParameters(rabbitmq_host, rabbitmq_port, rabbitmq_vhost, credentials)
                connection = pika.BlockingConnection(parameters)
                channel = connection.channel()
                channel.basic_consume(rabbitmq_tax_response_queue, self.get_tax_record_message)
                try:
                    channel.start_consuming()
                except KeyboardInterrupt:
                    channel.stop_consuming()
                    logger.info("Py_Integrator: Consuming interrupted.")
                connection.close()
                break
            except Exception as e:
                logger.info("Py_Integrator: Caught an exception in rabbitmq consumer. Retrying" + str(e))
                continue
            except pika.exceptions.AMQPConnectionError:
                print("Py_Integrator: Connection was closed, retrying...")
                continue
