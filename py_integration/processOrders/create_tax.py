import py_integration.processOrders.db_access as dba
import py_integration.configurations.environment_properties as prop
from py_integration.connections.rabbitMQ_connection import RabbitMQConnection
from collections import defaultdict
from datetime import datetime
from avalara.client import AvataxClient
import json
import pika
from datetime import date
from py_integration.connections.db_connection import DB2Connection

logger = prop.logging.getLogger(__name__)

class CreateTax(object):
    def __init__(self, call_type, ukid):
        self.conn = None
        self.cursor = None
        self.ukid = ukid
        self.call_type = call_type
        self.type = ""
        self.companyCode = ""
        self.code = ""
        self.date = ""
        self.customerCode = ""
        self.purchaseOrderNo = ""
        self.shipFrom_line1 = ""
        self.shipFrom_line2 = ""
        self.shipFrom_line3 = ""
        self.shipFrom_city = ""
        self.shipFrom_region = ""
        self.shipFrom_country = ""
        self.shipFrom_postalCode = ""
        self.shipTo_line1 = ""
        self.shipTo_line2 = ""
        self.shipTo_line3 = ""
        self.shipTo_city = ""
        self.shipTo_region = ""
        self.shipTo_country = ""
        self.shipTo_postalCode = ""
        self.number = ""
        self.quantity = 0
        self.amount = ""
        self.taxCode = ""
        self.itemCode = ""
        self.lines_description = ""
        self.taxIncluded = ""
        self.commit = ""
        self.currencyCode = ""
        self.description = ""
        self.DocumentOrderInvoice = 0
        self.OrderType = ""
        self.CompanyKeyOrderNo = ""
        self.LineNumber = 0
        self.AddressNumber = ""
        self.AddressNumberShipTo = ""
        self.UniqueKeyIDInternal = ""
        self.UserId = ""
        self.ProgramId = ""
        self.UnitsTransactionQty = ""
        self.AmtPricePerUnit2 = ""
        self.TaxArea1 = ""
        self.Identifier2ndItem = ""
        self.DescriptionLine1 = ""
        self.DescriptionLine2 = ""
        self.DateUpdated = ""
        self.TimeOfDay = ""
        self.WorkStationId = ""
        self.EdiSuccessfullyProcess = ""
        self.AmountExtendedPrice = ""
        self.AddressLine19 = ""
        self.AddressLine29 = ""
        self.AddressLine39 = ""
        self.shipFromState = ""
        self.PhoneNumber1 = ""
        self.AddressLine1 = ""
        self.AddressLine2 = ""
        self.AddressLine3 = ""
        self.shipToState = ""
        self.PhoneNumber = ""
        self.CallType = ""
        self.OriginalPoSoNumber = 0
        self.OriginalOrderType = ""
        self.CompanyKeyOriginal = ""
        self.ActualStartDate = ""
        self.ActualStartTime = ""
        self.ActualEndDate = ""
        self.ActualEndTime = ""
        self.InvoiceDate = ""
        self.cur_date_time = ""
        self.orderCompany = ""
        self.totalAmount = 0
        self.totalTaxable = 0
        self.totalExempt = 0
        self.totalTax = 0
        self.id = 0
        self.code = 0
        self.companyId = ""
        self.date = ""
        self.status = ""
        self.type = ""
        self.orderType = ""
        self.orderId = 0
        self.lineId = 0
        self.atriumId = 0
        self.lineAmount = 0
        self.exemptAmount = 0
        self.taxableAmount = 0
        self.tax = 0
        self.sku = ""
        self.taxAuthority1 = ""
        self.taxRate1 = 0
        self.taxAuthority2 = 0
        self.taxRate2 = 0
        self.taxAuthority3 = 0
        self.taxRate3 = 0
        self.taxAuthority4 = 0
        self.taxAuthority5 = 0
        self.taxRate5 = 0
        self.taxAmount1 = 0
        self.taxAmount2 = 0
        self.taxAmount3 = 0
        self.taxAmount4 = 0
        self.taxAmount5 = 0
        self.cur_time = 0
        self.invoiceId = 0
        self.invoiceType = ""
        self.invoiceCompany = ""
        self.ev01 = ""
        self.itemCode_msg = ""
        self.payload_json = ""
        self.DateUpdated_julian = 0
    
    def pydate_to_julian_date (cls, std_date):
        try:
            if not std_date:
                return ""
            else:
                return "1" + str(std_date.strftime('%y')) + std_date.strftime('%j')
        except Exception as e:
            logger.info("Py_Integrator: Exception caught in pydate_to_julian_date, error: " + str(e))

    def julian_to_date(self, j_date):
        try:
            if j_date:
                date_obj = datetime.strptime(j_date, '%y%j').date()
                standard_date = date_obj.strftime("%m-%d-%Y")
                return standard_date
            else:
                return ""
        except Exception as e:
            logger.info("Py_Integrator: Exception caught in julian_to_date, error: " + str(e))
    def julian_to_doc_date(self, j_date):
        try:
            if j_date:
                date_obj = datetime.strptime(j_date, '%y%j').date()
                standard_date = date_obj.strftime("%Y-%m-%d %H:%M:%S.%f")
                return standard_date
            else:
                return ""
        except Exception as e:
            logger.info("Py_Integrator: Exception caught in julian_to_date, error: " + str(e))
    def process_order(self):
        logger.info("Py_Integrator: Call type received is: " + self.call_type + " and ukid is: " + self.ukid)
        self.conn = DB2Connection.get_db2_connection()
        self.cursor = self.conn.cursor()
        try:
            logger.info("Py_Integrator: Executing select query for F59AT006 table to create avalara json request.")
            db_query =  "select RDDOCO as DocumentOrderInvoice, RDDCTO as OrderType, RDKCOO as CompanyKeyOrderNo, " \
                        "RDLNID as LineNumber, RDAN8 as AddressNumber, RDSHAN as AddressNumberShipTo, RDUKID as UniqueKeyIDInternal," \
                        "RDUSER as UserId, RDPID as ProgramId, RDUORG as UnitsTransactionQty, RDUPRC as AmtPricePerUnit2, RDTXA1 as TaxArea1, " \
                        "RDLITM as Identifier2ndItem, RDDSC1 as DescriptionLine1, RDDSC2 as DescriptionLine2, RDUPMJ as DateUpdated, " \
                        "RDTDAY as TimeOfDay, RDJOBN as WorkStaLNIDtionId, RDEDSP as EdiSuccessfullyProcess, RDAEXP as AmountExtendedPrice, " \
                        "RDADDRLIN1 as AddressLine19, RDADDRLIN2 as AddressLine29, RDADDRLIN3 as AddressLine39, RD55ADDZ as ZipCodePostal, " \
                        "RD55CTY1 as City, RD55ADDS as State, RD55CTR as Country, RDPH2 as PhoneNumber1, RDADD1 as AddressLine1, RDADD2 as AddressLine2, " \
                        "RDADD3 as AddressLine3, RDADDZ as ZipCodePostal, RDCTY1 as City, RDADDS as State, RDCTR as Country, RDPH1 as PhoneNumber, " \
                        "RD55CTYPE as CallType, RDOORN as OriginalPoSoNumber, RDOCTO as OriginalOrderType, RDOKCO as CompanyKeyOriginal, " \
                        "RDWASJDT as ActualStartDate, RDWWAST as ActualStartTime, RDWAEJDT as ActualEndDate, RDWWAET as ActualEndTime, RDIVD as InvoiceDate " \
                        "from " + prop.db2_library + ".F59AT006 where RDUKID = " + self.ukid + " and RD55CTYPE = '" + self.call_type + "' " \
                        "order by RDDOCO, RDKCOO, RDDCTO"
            logger.info("Py_Integrator: Select query for F59AT006 table to create avalara json request is: " + str(db_query))
            logger.info("Py_Integrator: Calling 'dba.ReadDataFromDB2.read_data_from_ibm_db2(db_query)' from create tax to read records from DB2 table F59AT006.")
            self.data = dba.ReadDataFromDB2.read_data_from_ibm_db2(db_query)
            if self.data:
                logger.info("Py_Integrator: Received data records from table F59AT006: " + str(self.data))
            else:
                logger.error("Py_Integrator: No data received from db, Exiting...")
                exit(0)
            self.taxIncluded = 'true' if self.call_type == 'TAXGETAR' or self.call_type == 'TAXPOSTAR' else 'false'
            self.commit = 'true' if self.call_type == 'TAXPOSTAR' else 'false'
            payload_dict = defaultdict(list)
            lines = []
            l = set()
            for r in self.data:
                l.add(str(r[0]))
            l = list(l)
            for inv in l:
                for row in self.data:
                    if inv == str(row[0]):
                        payload_dict[inv].append(row)
            for k, v in payload_dict.items():
                for item in v:
                    self.orderId = 0 if not item[0] else item[0]
                    self.orderType = "" if not item[1] else item[1]
                    self.CompanyKeyOrderNo = 0 if not item[2] else item[2]
                    self.LineNumber = 0 if not item[3] else item[3]
                    self.AddressNumber = 0 if not item[4] else item[4]
                    self.AddressNumberShipTo = 0 if not item[5] else item[5]
                    self.UniqueKeyIDInternal = 0 if not item[6] else item[6]
                    self.UserId = "" if not item[7] else item[7]
                    self.ProgramId = "" if not item[8] else item[8]
                    self.UnitsTransactionQty = 0 if not item[9] else item[9]
                    self.AmtPricePerUnit2 = 0 if not item[10] else item[10]
                    self.TaxArea1 = "" if not item[11] else item[11]
                    self.Identifier2ndItem = "" if not item[12] else item[12]
                    self.DescriptionLine1 = "" if not item[13] else item[13]
                    self.DescriptionLine2 = "" if not item[14] else item[14]
                    self.DateUpdated_julian = 0 if not item[15] else int(item[15])
                    self.DateUpdated = 0 if not item[15] else self.julian_to_date(str(int(item[15]))[1:])
                    self.TimeOfDay = 0 if not item[16] else item[16]
                    self.WorkStationId = "" if not item[17] else item[17]
                    self.EdiSuccessfullyProcess = 0 if not item[18] else item[18]
                    self.AmountExtendedPrice = 0 if not item[19] else item[19]
                    self.AddressLine19 = 0 if not item[20] else item[20]
                    self.AddressLine29 = "" if not item[21] else item[21]
                    self.AddressLine39 = "" if not item[22] else item[22]
                    self.shipFrom_postalCode = 0 if not item[23] else item[23]
                    self.shipFrom_city = "" if not item[24] else item[24]
                    self.shipFromState = "" if not item[25] else item[25]
                    self.shipFrom_country = "" if not item[26] else item[26]
                    self.PhoneNumber1 = 0 if not item[27] else item[27]
                    self.AddressLine1 = "" if not item[28] else item[28]
                    self.AddressLine2 = "" if not item[29] else item[29]
                    self.AddressLine3 = "" if not item[30] else item[30]
                    self.shipTo_postalCode = "" if not item[31] else item[31]
                    self.shipTo_city = "" if not item[32] else item[32]
                    self.shipToState = "" if not item[33] else item[33]
                    self.shipTo_country = "" if not item[34] else item[34]
                    self.PhoneNumber = 0 if not item[35] else item[35]
                    self.CallType = "" if not item[36] else item[36]
                    self.OriginalPoSoNumber = 0 if not str(item[37]).strip() else item[37]
                    self.OriginalOrderType = "" if not item[38] else item[38]
                    self.CompanyKeyOriginal = "" if not item[39] else item[39]
                    self.ActualStartDate = 0 if not item[40] else self.julian_to_date(str(int(item[40] ))[1:])
                    self.ActualStartTime = 0 if not item[41] else item[41]
                    self.ActualEndDate = 0 if not item[42] else self.julian_to_date(str(int(item[42]))[1:])
                    self.ActualEndTime = 0 if not item[43] else item[43]
                    self.InvoiceDate = str(datetime.now()) if not item[44] else str(self.julian_to_doc_date(str(int(item[44] ))[1:]))

                    line = {
                                "number" : float(self.LineNumber/1000),                #LNID/1000
                                "quantity" : float(self.UnitsTransactionQty/10000),    #UORG/10000
                                "amount" : float(self.AmountExtendedPrice/100),        #AEXP/100
                                "taxCode" : self.TaxArea1,                             #TXA1
                                "itemCode" : self.Identifier2ndItem,                   #LITM
                                "description" : self.DescriptionLine1,                 #DSC1
                                "taxIncluded" : self.taxIncluded                       #true if callType is 'TAXGETAR'/ 'TAXPOSTAR', otherwise false
                    }
                    lines.append(line)
                if self.call_type in('TAXGET', 'TAXGETP', 'TAXGETAR', 'TAXGETR', 'TAXGETPS'):
                    self.type = "SalesOrder"
                elif self.call_type in('TAXPOST', 'TAXPOSTAR'):
                    self.type = "SalesInvoice"
                self.cur_date_time = str(datetime.now())
                payload_data = {
                                "type": self.type,                                  #set as "SalesOrder" if callType is TAXGET, TAXGETP, TAXGETAR, TAXGETR
                                                                                    #set as "SalesInvoice" if callType is TAXPOST, TAXPOSTAR
                                "companyCode": str(self.CompanyKeyOrderNo),         #"DEFAULT", #F59AT006.RDKCOO
                                "code": str(self.orderId) + str(self.orderType) + str(self.CompanyKeyOrderNo), #RDDOCO ++ RDDCTO ++ RDKCOO
                                "date": self.InvoiceDate,                         #date and time
                                "customerCode": str(self.AddressNumber),            #AN8
                                "purchaseOrderNo": 0,                               #leave 0
                                "addresses": {
                                                "shipFrom": {
                                                                "line1":       self.AddressLine19,                          #ADDRLIN1
                                                                "line2":       self.AddressLine29,                          #ADDRLIN2
                                                                "line3":       self.AddressLine39,                          #ADDRLIN3
                                                                "city":        self.shipFrom_city,                          #55CTY1
                                                                "region":      self.shipFromState,                        #55ADDS
                                                                "country":     self.shipFrom_country,                       #55CTR
                                                                "postalCode":  str(self.shipFrom_postalCode).strip()        #55ADDZ
                                                },
                                                "shipTo": {
                                                              "line1":          self.AddressLine1,                          #ADD1
                                                              "line2":          self.AddressLine2,                          #ADD2
                                                              "line3":          self.AddressLine3,                          #ADD3
                                                              "city":           self.shipTo_city,                           #CTY1
                                                              "region":         self.shipToState,                         #ADDS
                                                              "country":        self.shipTo_country,                        #CTR
                                                              "postalCode":     str(self.shipTo_postalCode).strip()         #ADDZ
                                                }
                                },
                                "lines": lines,
                                'commit': self.commit,
                                'currencyCode': '',
                                'description': '',
                }
                self.tax_commit_payload = {
	                                        "invoiceNumber": int(self.orderId),
	                                        "invoiceType": str(self.orderType),
	                                        "invoiceCompany": str(self.CompanyKeyOrderNo)
                                          }
                payload_data = json.dumps(payload_data, indent=4)
                self.payload_json = payload_data
                payload_data = json.loads(payload_data)
                if self.call_type in('TAXGET', 'TAXGETPS', 'TAXGETAR', 'TAXGETR', 'TAXASYNC'):
                    logger.info("Py_Integrator: Avalara Request payload data: " + str(self.payload_json))
                    logger.info("Py_Integrator: Requesting Avalara sdk with request payload data.")
                    client = AvataxClient(prop.avalara_app_name, prop.avalara_app_version, prop.avalara_machine_name, prop.avalara_environment)
                    client = client.add_credentials(prop.avalara_username, prop.avalara_password)   # temporary creds ('2005976301', 'BD130133D16B975F')
                    transaction_response = client.create_transaction(payload_data)
                    transaction_response = json.dumps(transaction_response.json(), indent=4)
                    message_record = json.loads(transaction_response)
                    self.insert_avalara_tax_info_to_db2_tables(message_record)
                elif self.call_type in('TAXPOST', 'TAXGETP', 'TAXCOMMIT', 'TAXPOSTAR'):
                    currentDate = date.today()
                    self.sys_date_julian = self.pydate_to_julian_date(currentDate)
                    now = datetime.now()
                    now.strftime("%H:%M:%S")
                    logger.info("Py_Integrator: JSON to queue: " + str(self.payload_json))
                    self.cur_time = int("".join(now.strftime("%H:%M:%S").split(":")))
                    if self.call_type == 'TAXCOMMIT':
                        tax_exchange = prop.rabbitmq_tax_commit_exchange
                        tax_routing_key = prop.rabbitmq_tax_commit_routing_key
                        map_payload = json.dumps(self.tax_commit_payload, indent=4)
                    else:
                        tax_exchange = prop.rabbitmq_tax_request_exchange
                        tax_routing_key = prop.rabbitmq_tax_request_routing_key
                        map_payload = self.payload_json
                    
                    logger.info("Py_Integrator: Inserting the avalara request json into rabbitmq tax queue for call type: " + self.call_type)
                    logger.info("Py_Integrator: payload_json is: " + str(map_payload))
                    try:
                        channel = RabbitMQConnection.get_connection()
                        channel.basic_publish(exchange=tax_exchange, routing_key=tax_routing_key,
                                              body=map_payload, properties=pika.BasicProperties(content_type="application/json",
                                              delivery_mode=2))
                        logger.info("Py_Integrator: Msg successfully inserted to TAX Queue.")
                    except Exception as e:
                        logger.info("Py_Integrator: Message not inserted successfully into RabbitMQ TAX Queue. Error: " + str(e))
                        exit(1)
                    logger.info("Py_Integrator: After inserting avalara request to rabbitmq tax queue, now updating RDEDSP to Y into F59AT006 table.")
                    update_query = "update " + prop.db2_library + ".F59AT006 set RDEDSP = 'Y', RDUPMJ = " + str(self.sys_date_julian) + \
                    ", RDTDAY = " + str(self.cur_time) + " where RDUKID = " + str(self.ukid) + " and RD55CTYPE = '" + self.call_type + \
                    "' and RDDCTO = '" + str(self.orderType) + "' and RDDOCO = " + str(self.orderId) + " and RDKCOO = '" + \
                    str(self.CompanyKeyOrderNo) + "'"
                    logger.info("Py_Integrator: The update query for F59AT006 table: " + str(update_query))
                    self.cursor.execute(update_query)
                    self.cursor.commit()
                    logger.info("Py_Integrator: Update to table F59AT006 is success.")
                    
                lines = []
        except Exception as e:
            logger.info("Py_Integrator: Exiting...Caught an exception in process order in tax create, error: " + str(e))
            exit(1)
        finally:
            self.cursor.close()
            self.conn.close()
            pass

    def insert_avalara_tax_info_to_db2_tables(self, message_record):
        try:
            conn = DB2Connection.get_db2_connection()
            cursor = conn.cursor()
            if self.call_type == 'TAXASYNC':
                message_record = json.loads(message_record.decode("utf8"))
            logger.info("Py_Integrator: Avalara sdk response json: " + str(message_record))
            logger.info("Py_Integrator: call_type: " + str(self.call_type))
            lines_length = len(message_record.get("lines"))
            c = 0
            while c < lines_length:
                details_list = message_record.get("lines")[c].get("details") if message_record.get("lines")[c].get("details") else []
                tr_dict = 	{
                                "taxRate1":0,
                                "taxRate2":0,
                                "taxRate3":0,
                                "taxRate4":0,
                                "taxRate5":0,
                            }
                tax_auth_dict = {
                                    "taxAuthority1":0,
                                    "taxAuthority2":0,
                                    "taxAuthority3":0,
                                    "taxAuthority4":0,
                                    "taxAuthority5":0,
                                }
                tax_amt_dict = 	{
                                    "taxAmount1":0,
                                    "taxAmount2":0,
                                    "taxAmount3":0,
                                    "taxAmount4":0,
                                    "taxAmount5":0,
                                }
                juris_dict  =   {
                                    "jurisName1": "",
                                    "jurisName2": "",
                                    "jurisName3": "",
                                    "jurisName4": "",
                                    "jurisName5": "",
                                }
                tax_nm_dict =   {
                                    "taxName1": "",
                                    "taxName2": "",
                                    "taxName3": "",
                                    "taxName4": "",
                                    "taxName5": "",
                                }
                i = 0
                while i < len(details_list):
                    tr_dict["taxRate" + str(i+1)] = details_list[i].get("rate", 0)
                    tax_auth_dict["taxAuthority" + str(i+1)] = details_list[i].get("taxName", "")
                    tax_amt_dict["taxAmount" + str(i+1)] = details_list[i].get("tax", 0)
                    juris_dict["jurisName" + str(i+1)] = details_list[i].get("jurisName", "")
                    tax_nm_dict["taxName" + str(i+1)] = details_list[i].get("taxName", "")
                    i += 1

                self.id                     = str(message_record.get("id", 0))
                self.code                   = str(message_record.get("code", 0))
                self.companyId              = message_record.get("companyId", "")
                self.date                   = message_record.get("date", "")
                self.status                 = message_record.get("status", "")
                self.type                   = message_record.get("type", "")
                self.quantity               = message_record.get("lines")[c].get("quantity", 0)
                self.lineId                 = float(message_record.get("lines")[c].get("lineNumber", 1))
                self.atriumId               = message_record.get("customerCode", 0)
                self.lineAmount             = message_record.get("lines")[c].get("lineAmount", 0)
                self.exemptAmount           = message_record.get("lines")[c].get("exemptAmount", 0)
                self.taxableAmount          = message_record.get("lines")[c].get("taxableAmount", 0)
                self.tax                    = message_record.get("lines")[c].get("tax", 0)
                self.sku                    = message_record.get("sku", "")
                self.taxAuthority1          = tax_auth_dict.get("taxAuthority1", 0)
                self.taxRate1               = tr_dict.get("taxRate1", 0)
                self.taxAuthority2          = tax_auth_dict.get("taxAuthority2", 0)
                self.taxRate2               = tr_dict.get("taxRate2", 0)
                self.taxAuthority3          = tax_auth_dict.get("taxAuthority3", 0)
                self.taxRate3               = tr_dict.get("taxRate3", 0)
                self.taxAuthority4          = tax_auth_dict.get("taxAuthority4", 0)
                self.taxRate4               = tr_dict.get("taxRate4", 0)
                self.taxAuthority5          = tax_auth_dict.get("taxAuthority5", 0)
                self.taxRate5               = tr_dict.get("taxRate5", 0)
                self.taxAmount1             = tax_amt_dict.get("taxAmount1", 0)
                self.taxAmount2             = tax_amt_dict.get("taxAmount2", 0)
                self.taxAmount3             = tax_amt_dict.get("taxAmount3", 0)
                self.taxAmount4             = tax_amt_dict.get("taxAmount4", 0)
                self.taxAmount5             = tax_amt_dict.get("taxAmount5", 0)

                self.orderCompany           = message_record.get("orderCompany", "")
                self.totalAmount            = message_record.get("totalAmount", 0)
                self.totalTaxable           = message_record.get("totalTaxable", 0)
                self.totalExempt            = message_record.get("totalExempt", 0)
                self.totalTax               = message_record.get("totalTax", 0)
                self.invoiceId              = message_record.get("invoiceId", 0)
                self.invoiceType            = message_record.get("invoiceType", "")
                self.invoiceCompany         = message_record.get("invoiceCompany", "")
                self.itemCode_msg           = message_record.get("lines")[c].get("itemCode", "")
                
                self.quantity = self.quantity if self.taxableAmount >= 0 else -abs(self.quantity)
                
                if self.call_type == 'TAXASYNC':
                    self.codeLength = len(message_record.get("code", ""))
                    self.orderInvoiceNumber = message_record.get("code", "")[:(self.codeLength-7)]
                    self.orderInvoiceType = message_record.get("code", "")[(self.codeLength-7):(self.codeLength-5)]
                    self.orderInvoiceCompany = message_record.get("code", "")[(self.codeLength-5):]
                    if self.orderInvoiceType[0] != 'R': #order logic
                        self.orderId = self.orderInvoiceNumber
                        self.orderType = self.orderInvoiceType
                        self.CompanyKeyOrderNo = self.orderInvoiceCompany
                    else:
                        self.orderId = self.orderInvoiceNumber
                        self.orderType = self.orderInvoiceType
                        self.CompanyKeyOrderNo = self.orderInvoiceCompany
                        self.CompanyKeyOriginal = ''
                        self.OriginalOrderType = ''
                        self.OriginalPoSoNumber = str(0)
                                                
                        
                
                logger.info("Py_Integrator: call_type : " + str(self.call_type) + " and orderType " + str(self.orderType))
                if self.call_type in('TAXGET', 'TAXGETPS', 'TAXGETAR', 'TAXGETR'):
                    self.ev01 = 'P' if self.call_type == 'TAXGETP' or self.call_type == 'TAXGETPS' else ""
                elif self.call_type == 'TAXASYNC':
                    self.ev01 = 'P' if self.orderType[0] != 'R' else ""
                currentDate = date.today()
                self.sys_date_julian = self.pydate_to_julian_date(currentDate)
                now = datetime.now()
                now.strftime("%H:%M:%S")
                self.cur_time = int("".join(now.strftime("%H:%M:%S").split(":")))
                if self.call_type in('TAXGET', 'TAXGETPS', 'TAXGETAR', 'TAXGETR', 'TAXASYNC'):
                    if self.orderType[0] != 'R' and self.call_type != 'TAXGETAR':
                        logger.info("Py_Integrator: Inserting record extracted from Avalara response json into details table F59AT003.")
                        detail_insert_query = "insert into " + prop.db2_library + ".F59AT003 (ODCO, ODMCU, ODOBJ," \
                                                                                  " ODSUB, ODDCTO, ODDOCO, ODSFXO," \
                                                                                  " ODLNID, ODAN8, ODECST, ODAEXP," \
                                                                                  " ODATXN, ODATXA, ODNRTA, ODSTAM," \
                                                                                  " ODTXA1, ODEXR1, ODEXR2, ODTRDJ," \
                                                                                  " ODDGL, ODDSVJ, ODTX, ODITM," \
                                                                                  " ODLITM, ODAITM, ODLNTY, ODUORG," \
                                                                                  " ODUOM, ODFVTY, ODTA1, ODTXR1," \
                                                                                  " ODTA2, ODTXR2, ODTA3, ODTXR3," \
                                                                                  " ODTA4, ODTXR4, ODTA5, ODTXR5," \
                                                                                  " ODSTA1, ODSTA2, ODSTA3, ODSTA4," \
                                                                                  " ODSTA5, ODPRT1, ODOVTX, ODTXVA," \
                                                                                  " ODTXVR, ODPOST, ODICU, ODICUT," \
                                                                                  " ODGDVL, ODCRCD, ODCRR, ODSQNO," \
                                                                                  " ODAG, ODUPMJ, ODUPMT, ODPID," \
                                                                                  " ODUSER, ODJOBN, ODAPD, ODTAXE," \
                                                                                  " ODDOC, ODKCOO, ODPROCFL, ODADJFL," \
                                                                                  " ODTOFL, ODEV01, OD$9ATJURN1," \
                                                                                  " OD$9ATJURN2, OD$9ATJURN3," \
                                                                                  " OD$9ATJURN4, OD$9ATJURN5," \
                                                                                  " OD$9ATAXNM1, OD$9ATAXNM2," \
                                                                                  " OD$9ATAXNM3, OD$9ATAXNM4," \
                                                                                  " OD$9ATAXNM5) values(?, ?, ?, ?," \
                                                                                  " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," \
                                                                                  " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," \
                                                                                  " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," \
                                                                                  " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," \
                                                                                  " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," \
                                                                                  " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," \
                                                                                  " ?, ?, ?, ?, ?, ?, ?, ?, ?)"
                        detail_insert_query_values = ('', '', '', '', self.orderType, int(self.orderId), str('000'),
                                                      int(format(self.lineId * 1000, '.0f')),self.atriumId, 0,
                                                      int(format(self.lineAmount * 100, '.0f')),
                                                      int(format(self.exemptAmount * 100, '.0f')),
                                                      int(format(self.taxableAmount * 100, '.0f')), 0,
                                                      int(format(self.tax *100, '.0f')), '', '', '',
                                                      int(self.sys_date_julian), int(self.sys_date_julian), 0, '', 0,
                                                      self.itemCode_msg, self.itemCode_msg, '',
                                                      int(format(self.quantity * 10000, '.0f')), '', '', 0,
                                                      int(format(self.taxRate1 * 100000, '.0f')), 0,
                                                      int(format(self.taxRate2 * 100000, '.0f')), 0,
                                                      int(format(self.taxRate3 * 100000, '.0f')), 0,
                                                      int(format(self.taxRate4 * 100000, '.0f')), 0,
                                                      int(format(self.taxRate5 * 100000, '.0f')),
                                                      int(format(self.taxAmount1 * 100, '.0f')),
                                                      int(format(self.taxAmount2 * 100, '.0f')),
                                                      int(format(self.taxAmount3 * 100, '.0f')),
                                                      int(format(self.taxAmount4 * 100, '.0f')),
                                                      int(format(self.taxAmount5 * 100, '.0f')), '', '', 0, 0, '', 0,
                                                      '', 0, '', 0, 0, 0, int(self.sys_date_julian), self.cur_time, 'python',
                                                      'python', '', 0, 0, 0, self.CompanyKeyOrderNo, '', '', '',
                                                      self.ev01, juris_dict.get("jurisName1", ""),
                                                      juris_dict.get("jurisName2", ""),
                                                      juris_dict.get("jurisName3", ""),
                                                      juris_dict.get("jurisName4", ""),
                                                      juris_dict.get("jurisName5", ""),
                                                      tax_nm_dict.get("taxName1", ""), tax_nm_dict.get("taxName2", ""),
                                                      tax_nm_dict.get("taxName3", ""), tax_nm_dict.get("taxName4", ""),
                                                      tax_nm_dict.get("taxName5", ""))
                        logger.info("Py_Integrator: Details insert query for table F59AT003 is: " +
                                    str(detail_insert_query) + " and details insert query values are: ")
                        logger.info(detail_insert_query_values)
                        cursor.execute(detail_insert_query, detail_insert_query_values)
                        cursor.commit()
                        logger.info("Py_Integrator: Inserted the details message successfully into DB2 details table F59AT003.")
                        if c == (lines_length - 1):
                            logger.info("Py_Integrator: Inserting record extracted from Avalara response json into headers table F59AT002.")
                            header_insert_query = "insert into " + prop.db2_library + ".F59AT002 (OHDOCO, OHDCTO," \
                                                                                      " OHKCOO, OHEV01, OHAEXP," \
                                                                                      " OHATXA, OHATXN, OHSTAM," \
                                                                                      " OHTXR1, OHURRF, OHURCD," \
                                                                                      " OHURDT, OHURAB, OHUSER," \
                                                                                      " OHPID, OHUPMJ, OHTDAY," \
                                                                                      " OHJOBN) values(?, ?, ?, ?, ?," \
                                                                                      " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," \
                                                                                      " ?, ?, ?)"
                            header_insert_query_values = (int(self.orderId), self.orderType, self.CompanyKeyOrderNo,
                                                          self.ev01, int(format(self.totalAmount * 100, '.0f')),
                                                          int(format(self.totalTaxable * 100, '.0f')),
                                                          int(format(self.totalExempt * 100, '.0f')),
                                                          int(format(self.totalTax * 100, '.0f')), 0, '', '', 0, 0,
                                                          'python', 'python', int(self.sys_date_julian), int(self.cur_time), '')
                            logger.info("Py_Integrator: Headers insert query for table F59AT002 is: "
                                        + str(header_insert_query) + "and header_insert_query_values are: " )
                            logger.info(header_insert_query_values)
                            cursor.execute(header_insert_query, header_insert_query_values)
                            cursor.commit()
                            logger.debug("Py_Integrator: Inserted the headers record successfully into DB2 Headers table F59AT002.")
                            logger.info("Py_Integrator: After updating Headers table F59AT002, now updating RDEDSP to Y into F59AT006 table.")
                            update_query = "update " + prop.db2_library + ".F59AT006 set RDEDSP = 'Y', RDUPMJ = " + str(self.sys_date_julian) + \
                            ", RDTDAY = " + str(self.cur_time) + " where RDUKID = " + str(self.ukid) + " and RD55CTYPE = '" + self.call_type + \
                            "' and RDDCTO = '" + str(self.orderType) + "' and RDDOCO = " + str(self.orderId) + " and RDKCOO = '" + \
                            str(self.CompanyKeyOrderNo) + "'"
                            logger.info("Py_Integrator: The update query for F59AT006 table: " + str(update_query))
                            cursor.execute(update_query)
                            cursor.commit()
                            logger.info("Py_Integrator: Update query into DB2 table F59AT006 is success.")
                    else:
                        logger.info("Py_Integrator: Inserting record into headers table F59AT005.")
                        detail_insert_query = "insert into " + prop.db2_library + ".F59AT005 (IDCO, IDMCU, IDOBJ," \
                                                                                  " IDSUB, IDDCTO, IDDOCO, IDSFXO," \
                                                                                  " IDLNID, IDAN8, IDECST, IDAEXP," \
                                                                                  " IDEXR2, IDATXN, IDATXA, IDNRTA," \
                                                                                  " IDSTAM, IDTXA1, IDEXR1, IDTRDJ," \
                                                                                  " IDDGL, IDDSVJ, IDTX, IDITM," \
                                                                                  " IDLITM, IDAITM, IDLNTY, IDUORG," \
                                                                                  " IDUOM, IDFVTY, IDTA1, IDTXR1," \
                                                                                  " IDTA2, IDTXR2, IDTA3, IDTXR3," \
                                                                                  " IDTA4, IDTXR4, IDTA5, IDTXR5," \
                                                                                  " IDSTA1, IDSTA2, IDSTA3, IDSTA4," \
                                                                                  " IDSTA5, IDPRT1, IDOVTX, IDTXVA," \
                                                                                  " IDTXVR, IDPOST, IDICU, IDICUT," \
                                                                                  " IDGDVL, IDCRCD, IDCRR, IDSQNO," \
                                                                                  " IDAG, IDUPMJ, IDUPMT, IDPID," \
                                                                                  " IDUSER, IDJOBN, IDAPD, IDTAXE," \
                                                                                  " IDDOC, IDKCOO, IDPROCFL, IDADJFL," \
                                                                                  " IDTOFL, IDDCT, IDKCO," \
                                                                                  " ID$9ATJURN1, ID$9ATJURN2," \
                                                                                  " ID$9ATJURN3, ID$9ATJURN4," \
                                                                                  " ID$9ATJURN5, ID$9ATAXNM1," \
                                                                                  " ID$9ATAXNM2, ID$9ATAXNM3," \
                                                                                  " ID$9ATAXNM4, ID$9ATAXNM5)" \
                                                                                  " values(?, ?, ?, ?, ?, ?, ?, ?, ?," \
                                                                                  " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," \
                                                                                  " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," \
                                                                                  " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," \
                                                                                  " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," \
                                                                                  " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," \
                                                                                  " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," \
                                                                                  " ?, ?, ?, ?, ?)"
                        detail_insert_query_values = ('', '', '', '', self.OriginalOrderType,
                                                      self.OriginalPoSoNumber, '000',
                                                      int(format(self.lineId * 1000, '.0f')), int(self.atriumId), 0,
                                                      int(format(self.lineAmount * 100, '.0f')), '',
                                                      int(format(self.exemptAmount * 100, '.0f')),
                                                      int(format(self.taxableAmount * 100, '.0f')), 0,
                                                      int(format(self.tax *100, '.0f')), '', '',
                                                      int(self.sys_date_julian),
                                                      int(self.sys_date_julian), 0, '', 0, self.itemCode_msg,
                                                      self.itemCode_msg, '', int(format(self.quantity * 10000, '.0f')),
                                                      '', '', 0, int(format(self.taxRate1 * 100000, '.0f')), 0,
                                                      int(format(self.taxRate2 * 100000, '.0f')), 0,
                                                      int(format(self.taxRate3 * 100000, '.0f')), 0,
                                                      int(format(self.taxRate4 * 100000, '.0f')), 0,
                                                      int(format(self.taxRate5 * 100000, '.0f')),
                                                      int(format(self.taxAmount1 * 100, '.0f')),
                                                      int(format(self.taxAmount2 * 100, '.0f')),
                                                      int(format(self.taxAmount3 * 100, '.0f')),
                                                      int(format(self.taxAmount4 * 100, '.0f')),
                                                      int(format(self.taxAmount5 * 100, '.0f')), '', '', 0, 0, '', 0,
                                                      '', 0, '', 0, 0, 0, int(self.sys_date_julian), self.cur_time,
                                                      'python', 'python', '', 0, 0, int(self.orderId),
                                                      self.CompanyKeyOriginal, '', '', '', self.orderType,
                                                      self.CompanyKeyOrderNo, juris_dict.get("jurisName1", ""),
                                                      juris_dict.get("jurisName2", ""),
                                                      juris_dict.get("jurisName3", ""),
                                                      juris_dict.get("jurisName4", ""),
                                                      juris_dict.get("jurisName5", ""), tax_nm_dict.get("taxName1", ""),
                                                      tax_nm_dict.get("taxName2", ""), tax_nm_dict.get("taxName3", ""),
                                                      tax_nm_dict.get("taxName4", ""), tax_nm_dict.get("taxName5", ""))
                        logger.info("Py_Integrator: Details insert query for table F59AT005 is: " +
                                    str(detail_insert_query) + " and details insert query values are: ")
                        logger.info(detail_insert_query_values)
                        cursor.execute(detail_insert_query, detail_insert_query_values)
                        cursor.commit()
                        logger.debug("Py_Integrator: Inserted record successfully into DB2 details table F59AT005.")
                        if c == (lines_length - 1):
                            logger.info("Py_Integrator: Inserting record into headers table F59AT004.")
                            header_insert_query = "insert into " + prop.db2_library + ".F59AT004 (IHDOCO, IHDCTO," \
                                                                                      " IHKCOO, IHAEXP, IHATXA," \
                                                                                      " IHATXN, IHSTAM, IHTXR1," \
                                                                                      " IHURRF, IHURCD, IHURDT," \
                                                                                      " IHURAB, IHUSER, IHPID," \
                                                                                      " IHUPMJ, IHTDAY, IHJOBN," \
                                                                                      " IHDOC, IHDCT, IHKCO)" \
                                                                                      " values(?, ?, ?, ?, ?, ?, ?," \
                                                                                      " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," \
                                                                                      " ?, ?, ?)"
                            header_insert_query_values = (self.OriginalPoSoNumber, self.OriginalOrderType,
                                                          self.CompanyKeyOriginal,
                                                          int(format(self.totalAmount * 100, '.0f')),
                                                          int(format(self.totalTaxable * 100, '.0f')),
                                                          int(format(self.totalExempt * 100, '.0f')),
                                                          int(format(self.totalTax * 100, '.0f')), 0, '', '', 0, 0,
                                                          'python', 'python', int(self.sys_date_julian), int(self.cur_time), '',
                                                          int(self.orderId), self.orderType, self.CompanyKeyOrderNo)
                            logger.info("Py_Integrator: The headers insert query for F59AT004 table: " +
                                        str(header_insert_query) + " and headers insert query values are: ")
                            logger.info(header_insert_query_values)
                            cursor.execute(header_insert_query, header_insert_query_values)
                            cursor.commit()
                            logger.info("Py_Integrator: Insert into DB2 Headers table F59AT004 is success......")
                            logger.info("Py_Integrator: After updating Headers table F59AT004, now updating RDEDSP to Y into F59AT006 table.")
                            update_query = "update " + prop.db2_library + ".F59AT006 set RDEDSP = 'Y', RDUPMJ = " + str(self.sys_date_julian) + \
                            ", RDTDAY = " + str(self.cur_time) + " where RDUKID = " + str(self.ukid) + " and RD55CTYPE = '" + self.call_type + \
                            "' and RDDCTO = '" + str(self.orderType) + "' and RDDOCO = " + str(self.orderId) + " and RDKCOO = '" + \
                            str(self.CompanyKeyOrderNo) + "'"
                            #print(update_query)
                            logger.info("Py_Integrator: The update query for F59AT006 table: " + str(update_query))
                            cursor.execute(update_query)
                            cursor.commit()
                            logger.info("Py_Integrator: Update into DB2 table F59AT006 is success.")

                c += 1
        except Exception as e:
            logger.info("Py_Integrator: Exiting...Caught an exception in insert_avalara_tax_info_to_db2_tables in tax create, error: " + str(e))
            #exit(1)
