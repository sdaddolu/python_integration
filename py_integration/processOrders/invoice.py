import py_integration.processOrders.db_access as dba
from py_integration.processOrders.processOrder import *
import py_integration.configurations.environment_properties as prop
from collections import defaultdict
import py_integration.processOrders.insert_rabbitMQ as rb
from datetime import datetime



logger = prop.logging.getLogger(__name__)
class Invoice(object):
    def __init__(self, call_type, ukid):
        self.ukid = ukid
        self.call_type = call_type
        self.invoiceType = ""
        self.invoiceCompany = ""
        self.sourceSystem = ""
        self.sourceSystemOrderId = ""
        self.e1OrderId = 0
        self.e1OrdLine = 0
        self.sku = ""
        self.quantity = 0
        self.itemAmount = 0
        self.sourceSystem = ""
        self.itemId = ""
        self.status = ""
        self.message_status = ""
        self.invoiceNum = 0
        self.items = []
        self.trackingNo = ""
        self.carrierCode = 0
        self.rep_code = 0
        self.rep_code_description = ""
        self.irep_code = 0
        self.irep_code_description = ""
        self.purchaseOrderNumber = ""
        self.UOM = ""
        self.lastStatus = ""
        self.nextStatus = ""
        self.lastStatusDescription = ""
        self.nextStatusDescription = ""
        self.additionalInfo = ""
        self.promisedDate = ""
        self.shipDate = ""
        self.invoiceDate = ""
        self.orderType = ""
        self.customerNumber = 0
        self.invoiceStatus = ""
        self.orderDate = ""
        self.itemName = ""
        self.data = None

    def julian_to_date(self, j_date):
        if j_date:
            date_obj = datetime.strptime(j_date, '%y%j').date()
            standard_date = date_obj.strftime("%m/%d/%Y")
            return standard_date
        else:
            return ""

    def process_order(self):
        db_query = "select sdkco as invoiceCompany, sddct as invoiceType, sddoc as invoiceNum, sddoco as e1OrderId, " \
                   "sdlnid/1000 as e1OrdLine, sdlitm as sku, sdsoqs/10000 as quantity, sdaexp/100 as itemAmount, " \
                   "SDOPDJ as promisedDate, SDADDJ as shipDate, SDIVD as invoiceDate, " \
                   "SDDCTO as orderType, SDAN8 as customerNumber, xr55srcsys as sourceSystem, xr55srdoco as " \
                   "sourceSystemOrderId, xr55srlnid as itemId, M256CSTRA as trackingNo, M2URAB as carrierCode, " \
                   "M2SLSM as rep_code, M2ALPH as rep_code_description, M2SLSP as irep_code, M2ALPH2 as " \
                   "irep_code_description, M2VR01 as purchaseOrderNumber, SDUOM as UOM, M2LTTR as lastStatus, " \
                   "M2NXTR as nextStatus, M2STDS as lastStatusDescription, M2LNDS as nextStatusDescription, " \
                   "SDSRP4 as additionalInfo, SDTRDJ as orderDate, SDDSC1 as itemName, SDUPRC/10000 as unitPrice, M255SROOID as internalSourceOrderId, M2DL011 as transactionId from " + prop.db2_library + \
                   ".F5542008 inner join " + prop.db2_library + ".F4211 on m2doco = sddoco and m2litm = sdlitm " \
                    "and m2nxtr = sdnxtr left outer join " + prop.db2_library + ".F5642XEF on xrdoco = sddoco " \
                    "and xrlitm = sdlitm where m2ukid = " + self.ukid + " and sdnxtr = '590' " \
                    "and m2#eo = 'P' order by sddoco, M256CSTRA"
        self.data = dba.ReadDataFromDB2.read_data_from_ibm_db2(db_query)
        if self.data :
            logger.debug("Py_Integrator: Date available in db")
        else:
            logger.error("Py_Integrator: No data received from db, Exiting...")
            exit(0)
        order_types = ["SW", "SP", "SO"]
        payload_dict = defaultdict(list)
        payload_data = {
                "sourceSystem":"",
                "sourceSystemOrderId": "",
                "invoiceNumber": 0,
                "invoiceType": "",
                "invoiceCompany": "",
                "invoiceStatus": "",
                "e1OrderId": 0,
                "customerNumber": 0,
                "rep_code": 0,
                "rep_code_description": "",
                "irep_code": 0,
                "irep_code_description": "",
                "purchaseOrderNumber": "",
                "orderType": "",
                "orderDate": 0,
                "sourceSystemInternalOrderId": "",
                "sourceSystemMonetaryTransactionId":[],
                "items": []
        }
        l = set()
        for r in self.data:
            l.add(str(r[2]))
        l = list(l)
        for inv in l:
            for row in self.data:
                if inv == str(row[2]):
                    payload_dict[inv].append(row)
        for k, v in payload_dict.items():
            for item in v:
                self.invoiceCompany = "" if not item[0] else item[0]
                self.invoiceType = "" if not item[1] else item[1]
                self.invoiceNum = 0 if not item[2] else item[2]
                self.e1OrderId = 0 if not item[3] else item[3]
                self.e1OrdLine = 0.0 if not item[4] else float(item[4])
                self.sku = "" if not item[5] else item[5]
                self.quantity = 0 if not item[6] else item[6]
                self.itemAmount = 0 if not item[7] else item[7]
                self.promisedDate = 0 if not item[8] else item[8]
                self.shipDate = 0 if not item[9] else item[9]
                self.invoiceDate = 0 if not item[10] else item[10]
                self.orderType = "" if not item[11] else item[11]
                self.customerNumber = 0 if not item[12] else item[12]
                self.sourceSystem = "" if not item[13] else item[13]
                self.sourceSystemOrderId = "" if not item[14] else item[14]
                self.itemId = "" if not item[15] else item[15]
                self.trackingNo = "" if not item[16] else item[16]
                self.carrierCode = 0 if not item[17] else item[17]
                self.rep_code = 0 if not item[18] else item[18]
                self.rep_code_description = "" if not item[19] else item[19]
                self.irep_code = 0 if not item[20] else item[20]
                self.irep_code_description = "" if not item[21] else item[21]
                self.purchaseOrderNumber = "" if not item[22] else item[22]
                self.UOM = "" if not item[23] else item[23]
                self.lastStatus = "" if not item[24] else item[24]
                self.nextStatus = "" if not item[25] else item[25]
                self.lastStatusDescription = "" if not item[26] else item[26]
                self.nextStatusDescription = "" if not item[27] else item[27]
                self.additionalInfo = "" if not item[28] else item[28]
                self.orderDate = 0 if not item[29] else item[29]
                self.itemName = "" if not item[30] else item[30]
                self.unitPrice = 0 if not item[31] else item[31]
                self.sourceSystemInternalOrderId = "" if not item[32] else item[32]
                self.transactionId = "" if not item[33].replace(" ","") else item[33].replace(" ","")

                # Modify to map the given json.
                self.promisedDate = self.julian_to_date(str(int(self.promisedDate))[1:])
                self.shipDate = self.julian_to_date(str(int(self.shipDate))[1:])
                self.invoiceDate = self.julian_to_date(str(int(self.invoiceDate))[1:])
                self.orderDate = self.julian_to_date(str(int(self.orderDate))[1:])
                self.sourceSystemMonetaryTransactionId = self.transactionId.split(",")

                item_dict = {
                    "e1LineId": float(self.e1OrdLine),
                    "itemId": str(self.itemId).strip(),
                    "itemName": str(self.itemName).strip(),
                    "sku": self.sku.strip(),
                    "quantity": float(self.quantity),
                    "UOM": self.UOM,
                    "itemAmount": float(self.itemAmount),
                    "promisedDate": self.promisedDate,
                    "shipDate": self.shipDate,
                    "invoiceDate": self.invoiceDate,
                    "lastStatus": self.lastStatus.strip(),
                    "lastStatusDescription": self.lastStatusDescription.strip(),
                    "nextStatus": self.nextStatus,
                    "nextStatusDescription": self.nextStatusDescription.strip(),
                    "additionalInfo": self.additionalInfo,
                    "unitPrice": float(self.unitPrice)
                }

                s = str(int(self.e1OrderId))
                inv = str(int(self.invoiceNum))
                if inv != payload_data.get("invoiceNumber"):
                    payload_data["sourceSystem"] = self.sourceSystem.strip()
                    payload_data["sourceSystemOrderId"] = self.sourceSystemOrderId.strip()
                    payload_data["invoiceNumber"] = inv
                    payload_data["invoiceType"] = self.invoiceType.strip()
                    payload_data["invoiceCompany"] = self.invoiceCompany.strip()
                    if self.orderType in order_types:
                        self.invoiceStatus = "I"
                    else:
                        self.invoiceStatus = "CI"
                    payload_data["invoiceStatus"] = self.invoiceStatus
                    payload_data["e1OrderId"] = int(s)
                    payload_data["customerNumber"] = int(self.customerNumber)
                    payload_data["rep_code"] = str(self.rep_code)
                    payload_data["rep_code_description"] = self.rep_code_description.strip()
                    payload_data["irep_code"] = str(self.irep_code)
                    payload_data["irep_code_description"] = self.irep_code_description.strip()
                    payload_data["purchaseOrderNumber"] = str(self.purchaseOrderNumber).strip()
                    payload_data["orderType"] = self.orderType
                    payload_data["orderDate"] = self.orderDate
                    payload_data["sourceSystemInternalOrderId"] = str(self.sourceSystemInternalOrderId).strip()
                    payload_data["sourceSystemMonetaryTransactionId"] = self.sourceSystemMonetaryTransactionId
                    payload_data["items"].append(item_dict)
                else:
                    payload_data["items"].append(item_dict)
            logger.debug("Py_Integrator: Start: Writing to RabbitMQ, "
                          "invoiceNumber: " + str(payload_data["invoiceNumber"]) + ", invoiceType: " +
                          str(payload_data["invoiceType"]) + ", invoiceCompany: " +
                          str(payload_data["invoiceCompany"]) + "\n")
            rb.InsertToRabbitMQ.insert_into_rabbitmq(payload_data, self.nextStatus)
            if rb.InsertToRabbitMQ.msg_status == 'Y':
                logger.debug("Py_Integrator: Successfully inserted message to RabbitMQ")
            else:
                logger.error("Py_Integrator: Failed to insert message on to RabbitMQ")

            dba.UpdateDB2.update_db2(rb.InsertToRabbitMQ.msg_status, self.call_type, payload_data["invoiceNumber"],
                                         self.sku.strip(), self.nextStatus, self.ukid)
            payload_data = {
                    "sourceSystem":"",
                    "sourceSystemOrderId": "",
                    "invoiceNumber": "",
                    "invoiceType": "",
                    "invoiceCompany": "",
                    "invoiceStatus": "",
                    "e1OrderId": 0,
                    "customerNumber": 0,
                    "rep_code": "",
                    "rep_code_description": "",
                    "irep_code": "",
                    "irep_code_description": "",
                    "purchaseOrderNumber": "",
                    "orderType": "",
                    "orderDate": "",
                    "sourceSystemInternalOrderId": "",
                    "sourceSystemMonetaryTransactionId": [],
                    "items": []
               }
