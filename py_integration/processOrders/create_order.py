import pika
import json
from py_integration.connections.rabbitMQ_connection import RabbitMQConnection
from py_integration.connections.db_connection import *
from datetime import datetime
from datetime import date
import logging

logger = logging.getLogger(__name__)


class CreateOrder:
    def __init__(self, call_type, ukid):
        self.call_type = call_type
        self.ukid = ukid
        self.sourceCustomerId = 0
        self.sourceOrderId = ""
        self.sourceSystem = ""
        self.jdeOrderId = 0
        self.orderDate = ""
        self.orderType = ""
        self.accountName = ""
        self.billingAddressId = 0
        self.billingAddress1 = ""
        self.billingAddress2 = ""
        self.billingAddress3 = ""
        self.billingCity = ""
        self.billingState = ""
        self.billingPostalCode = ""
        self.billingCountry = ""
        self.billingAddressType = ""
        self.billingCompany = ""
        self.billingTelephone = ""
        self.billingFirstName = ""
        self.billingLastName = ""
        self.shippingAddressId = 0
        self.shippingAddress1 = ""
        self.shippingAddress2 = ""
        self.shippingAddress3 = ""
        self.shippingCity = ""
        self.shippingState = ""
        self.shippingPostalCode = ""
        self.shippingCountry = ""
        self.shippingAddressType = ""
        self.shippingTelephone = ""
        self.shippingFirstName = ""
        self.shippingLastName = ""
        self.carrier = "0"
        self.freightHandling = ""
        self.deliveryInstructions = ""
        self.deliveryInstructions2 = ""
        self.purchaseOrderNumber = ""
        self.currency = ""
        self.paymentTerms = ""
        self.orderTakenBy = 0
        self.orderedBy = ""
        self.businessUnit = ""
        self.orderCompany = ""
        self.lineOfBusiness = ""
        self.authorizationNumber = ""
        self.commissionCodeRep = ""
        self.commissionCodeIRep = ""
        self.commissionCodeDescriptionRep = ""
        self.hasEDIOrderLines = ""
        self.lastTransactionId = 0
        self.pickSlipNumber = 0
        self.ediBatchNumber = 0
        self.orderTotal = 0.0
        self.totalTaxable = 0
        self.totaltaxExempt = 0.0
        self.totalTax = 0
        self.lineId = 0
        self.sourceOrderItemId = 0
        self.sku = ""
        self.itemName = ""
        self.promisedDate = ""
        self.requestedDeliveryDate = ""
        self.quantity = 0
        self.unitPrice = 0
        self.suggestedRetailPrice = 0
        self.discount = 0
        self.unitOfMeasure = ""
        self.detailBusinessUnit = ""
        self.orderDetailCompany = ""
        self.detailCarrier = 0
        self.isEDIItem = True
        self.pickSlipNumber = 0
        self.ediBatchNumber = ""
        self.lineAmount = 0
        self.taxableAmount = 0
        self.taxExempt = 0
        self.tax = 0
        self.taxRate = 0
        self.newPrice = 0
        self.process_flag = "N"
        self.amtPricePerUnit = 0


        self.payload = {
                            "sourceCustomerId": 82899,                                         #number
                            "sourceOrderId": 1103326,                                          #string
                            "sourceSystem": "MAGENTO",                                         #string
                            "jdeOrderId": 16256164,                                            #number
                            "orderDate": "12/04/2022",                                         #MM-DD-YYYY
                            "orderType": "SW",                                                 #string
                            "accountName": "ROSEANNE NENNINGER",                               #string
                            "billingAddress": {
                                                    "addressId": 207068,                       #number
                                                    "address1": "109 RANDALL AVE",             #string
                                                    "address2": "",                            #string
                                                    "address3": "",                            #string
                                                    "city": "PORT JEFFERSON",                  #string
                                                    "state": "NY",                             #string
                                                    "postalCode": "11777",                     #string
                                                    "country": "US",                           #string
                                                    "addressType": "billing",                  #string
                                                    "company": "ROSEANNE NENNINGER",           #string
                                                    "telephone": "6318342502",                 #string
                                                    "firstName": "ROSEANNE",                   #string
                                                    "lastName": "NENNINGER"                    #string
                            },
                            "shippingAddress": {
                                                    "addressId": 12172533,                     #number
                                                    "address1": "116 HAMLET DR",               #string
                                                    "address2": "",                            #string
                                                    "address3": "",                            #string
                                                    "city": "MOUNT SINAI",                     #string
                                                    "state": "NY",                             #string
                                                    "postalCode": "11766-3004",                #string
                                                    "country": "US",                           #string
                                                    "addressType": "shipping",                 #string
                                                    "telephone": "6318342502",                 #string
                                                    "firstName": "Cally",                      #string
                                                    "lastName": "Kokolakis",                   #string
                                                    "carrier": "0",                            #string
                                                    "freightHandling": "",                     #string
                                                    "deliveryInstructions": "test1",           #string
                                                    "deliveryInstructions2": "test2"           #string
                            },
                            "purchaseOrderNumber": "Test",                                     #string
                            "currency": "USD",                                                 #string
                            "paymentTerms": "CC",                                              #string
                            "orderTakenBy": "MAGENTO",                                         #string
                            "orderedBy": "Magento",                                            #string
                            "businessUnit": "201100",                                          #string
                            "orderCompany": "00201",                                           #string
                            "lineOfBusiness": "USP",                                           #string
                            "authorizationNumber": "166305",                                   #string
                            "commissionCodeRep": "20627, MINNIE SALHOTRA",                     #string
                            "commissionCodeIRep": "88888, Atrium Innovations",                 #string
                            "commissionCodeDescriptionRep": "MINNIE SALHOTRA",                 #string
                            "hasEDIOrderLines": "true",                                        #string
                            "lastTransactionId": "43325778658",                                #string
                            "pickSlipNumber": 0,                                               #string
                            "ediBatchNumber": 8321259,                                         #number
                            "orderTotal": 75.6,                                                #number
                            "totalTaxable": 10.3,                                                 #number
                            "totaltaxExempt": 65.5,                                            #number
                            "totalTax": 1.2,                                                     #number
                            "salesOrderDetail": [ {
                                                        "lineId": 1,                           #number
                                                        "sourceOrderItemId": 6459491,          #number
                                                        "sku": "NS11",                         # string
                                                        "itemName": "NSK-SD (Nattokinase) 100 mg. 120's", #string
                                                        "promisedDate": "12/04/2022",          #MM-DD-YYYY
                                                        "requestedDeliveryDate": "12/04/2022", #MM-DD-YYYY
                                                        "quantity": 1,                         #number
                                                        "unitPrice": 65.6,                     #number
                                                        "suggestedRetailPrice": 131.2,         #number
                                                        "discount": 0,                         #number
                                                        "unitOfMeasure": "",                   #string
                                                        "detailBusinessUnit": "201202",        #string
                                                        "orderDetailCompany": "00201",         #string
                                                        "detailCarrier": "30056",              #string
                                                        "isEDIItem": "true",                   #boolean
                                                        "pickSlipNumber": 50555999,            #number
                                                        "ediBatchNumber": "8321259",           #string
                                                        "lineAmount": 65.6,                    #number
                                                        "taxableAmount": 10.4,                 #number
                                                        "taxExempt": 65.6,                     #number
                                                        "tax": 1.5,                            #number
                                                        "taxRate": 8.625,                      #number
                                                        "newPrice": 65.6                       #number
                                                    },
                                                    {
                                                        "lineId": 2,                           #number
                                                        "sourceOrderItemId": 6459492,          #number
                                                        "sku": "NS16",                         #string
                                                        "itemName": "NSK-SD (Nattokinase) 100 mg. 60's", #string
                                                        "promisedDate": "12/04/2022",          #MM-DD-YYYY
                                                        "requestedDeliveryDate": "12/04/2022", #MM-DD-YYYY
                                                        "quantity": 1,                         #number
                                                        "unitPrice": 65.6,                     #number
                                                        "suggestedRetailPrice": 131.2,         #number
                                                        "discount": 0,                         #number
                                                        "unitOfMeasure": "",                   #string
                                                        "detailBusinessUnit": "201202",        #string
                                                        "orderDetailCompany": "00201",         #string
                                                        "detailCarrier": "30056",              #string
                                                        "isEDIItem": "true",                   #boolean
                                                        "pickSlipNumber": 50555999,            #number
                                                        "ediBatchNumber": "8321259",           #string
                                                        "lineAmount": 65.6,                    #number
                                                        "taxableAmount": 0,                    #number
                                                        "taxExempt": 65.6,                     #number
                                                        "tax": 0,                              #number
                                                        "taxRate": 8.625,                      #number
                                                        "newPrice": 65.6                       #number
                                                    }
                                                ]
        }
    def process_special_char(self, s):
        if not s:
            return s
        new_str = ''
        i = 0
        s = str(s)
        while i < len(s):
            if s[i] == "'":
                new_str += '\'\''
                i += 1
                continue
            new_str += s[i]
            i += 1
        s = new_str
        return s
        
    def pydate_to_julian_date (cls, std_date):
        try:
            if not std_date:
                return ""
            else:
                return "1" + str(std_date.strftime('%y')) + std_date.strftime('%j')
        except Exception as e:
            print("Exception caught, error: ", e)
            logger.info("Py_Integrator: Exception caught in pydate_to_julian_date, error: " + str(e))

    def get_order_record_message(self, channel, method_frame, header_frame, message_body):
        logger.info("Py_Integrator: Received order record message:")
        logger.info("Py_Integrator: Order message: " + str(message_body) + "\nMessage header frame: " +
                    str(header_frame) + "\nmessage delivery_tag: " + str(method_frame.delivery_tag))
        #print(method_frame.delivery_tag)
        #print(message_body)
        #print(header_frame)
        message = message_body
        #print(message)
        message = json.loads(message.decode("utf8"))
        logger.info("Py_Integrator: After converting message to json: " + str(message))
        #print(message)
        channel.basic_ack(delivery_tag=method_frame.delivery_tag)
        status_flag = self.is_order_create_set_on()
        if status_flag == 'Y':
            try:
                logger.info("Py_Integrator: Calling insert_into_db2_order_staging_table to insert message record to insert into DB2 staging table.")
                self.insert_into_db2_order_staging_table(message_body)
                logger.info("Py_Integrator: Successfully inserted message into DB2 table.")
            except Exception as e:
                self.insert_to_order_DL_queue(message)
                #print(e)
                logger.info("Py_Integrator: Unable to write Order record for OrderId: " + self.sourceOrderId + "and Sourcesystem: " +
                self.sourceSystem + " to DB2 table, the message went to DL queue. Please investigate. Error: " + str(e))
        else:
            self.insert_to_order_DL_queue(message)
            logger.info("Py_Integrator: The Order message went to DL queue. The status flag in the config table for "
                         "Order is not set to 'Y', status flag is: " + str(status_flag))
            exit(0)

    def insert_to_order_DL_queue(self, payload):
        try:
            logger.info("Py_Integrator: Inserting message to DL Queue.")
            order_DL_exchange = rabbitmq_order_DL_exchange
            order_DL_routing_key = rabbitmq_order_DL_routing_key
            # order_DL_exchange = rabbitmq_order_exchange
            # order_DL_routing_key = rabbitmq_order_routing_key
            payload_json = json.dumps(payload, indent=4)
            logger.info("Py_Integrator: Payload json is: " + str(payload_json))
            channel = RabbitMQConnection.get_connection()
            channel.basic_publish(exchange=order_DL_exchange, routing_key=order_DL_routing_key,
                                  body=payload_json, properties=pika.BasicProperties(content_type="application/json",
                                  delivery_mode=2))
            print("Msg successfully inserted to DL Queue.")
            logger.info("Py_Integrator: Msg successfully inserted to DL Queue.")
        except Exception as e:
            logger.info("Py_Integrator: Message not inserted successfully into RabbitMQ DL Queue. Error: " + str(e))

    def insert_into_db2_order_staging_table(self, message_record):
        try:
            logger.info("Py_Integrator: Inside insert_into_db2_order_staging_table function.")
            message_record = json.loads(message_record.decode("utf8"))
            conn = DB2Connection.get_db2_connection()
            cursor = conn.cursor()
            self.jdeOrderId                     = str(message_record.get("jdeOrderId", 0))
            self.billingAddressId               = str(message_record.get("billingAddress").get("addressId", 0))
            self.sourceOrderId                  = message_record.get("sourceOrderId", "")
            self.sourceSystem                   = message_record.get("sourceSystem", "")
            self.process_flag                   = message_record.get("process_flag", "N")
            self.orderType                      = message_record.get("orderType", "")
            self.orderCompany                   = message_record.get("orderCompany", "")
            self.businessUnit                   = '{0: >12}'.format(message_record.get("businessUnit", ""))

            now = datetime.now()
            now.strftime("%H:%M:%S")
            currentTime = int("".join(now.strftime("%H:%M:%S").split(":")))
            currentDate = date.today()
            current_jl_date_Date = self.pydate_to_julian_date(currentDate)


            if message_record.get("salesOrderDetail"):
                for order_detail_dict in message_record.get("salesOrderDetail"):
                    self.lineId                         = str(format(order_detail_dict.get("lineId", 0) * 1000, '.0f'))
                    self.sourceOrderItemId              = str(order_detail_dict.get("sourceOrderItemId", 0))
                    self.sku                            = order_detail_dict.get("sku", "")
                    self.itemName                       = order_detail_dict.get("itemName", "")
                    if order_detail_dict.get("promisedDate"):
                        promisedDate_obj = datetime.strptime(order_detail_dict.get("promisedDate"), '%m-%d-%Y')
                        self.promisedDate = self.pydate_to_julian_date(promisedDate_obj.date())
                    else:
                        self.promisedDate = ""

                    self.requestedDeliveryDate          = order_detail_dict.get("requestedDeliveryDate", "")
                    self.quantity                       = str(format(order_detail_dict.get("quantity", 0.0) * 10000, '.0f'))
                    self.unitPrice                      = str(format(order_detail_dict.get("unitPrice", 0.0) * 10000, '.0f'))
                    self.suggestedRetailPrice           = str(format(order_detail_dict.get("suggestedRetailPrice", 0.0) * 10000, '.0f'))
                    self.discount                       = str(format(order_detail_dict.get("discount", 0.0) * 10000, '.0f'))
                    self.amtPricePerUnit                = str(order_detail_dict.get("amtPricePerUnit", 0))
                    self.unitOfMeasure                  = order_detail_dict.get("unitOfMeasure", "")
                    self.detailBusinessUnit             = '{0: >12}'.format(order_detail_dict.get("detailBusinessUnit", ""))
                    self.orderDetailCompany             = order_detail_dict.get("orderDetailCompany", "")
                    self.detailCarrier                  = order_detail_dict.get("detailCarrier") if order_detail_dict.get("detailCarrier") else 0
                    self.isEDIItem                      = order_detail_dict.get("isEDIItem", "")
                    is_edi_item                         = "DC" if self.isEDIItem == True else ""
                    self.pickSlipNumber                 = str(order_detail_dict.get("pickSlipNumber", 0))
                    self.ediBatchNumber                 = order_detail_dict.get("ediBatchNumber", "")
                    self.lineAmount                     = str(format(order_detail_dict.get("lineAmount", 0.0) * 100, '.0f'))
                    self.taxableAmount                  = str(format(order_detail_dict.get("taxableAmount", 0.0) * 100, '.0f'))
                    self.taxExempt                      = str(format(order_detail_dict.get("taxExempt", 0.0) * 100, '.0f'))
                    self.tax                            = str(format(order_detail_dict.get("tax", 0.0) * 100, '.0f'))
                    self.taxRate                        = str(format(order_detail_dict.get("taxRate", 0.0) * 1000, '.0f'))
                    self.newPrice                       = str(format(order_detail_dict.get("newPrice", 0.0) * 100, '.0f'))
                    
                    self.extendedPrice                  = self.taxableAmount if int(self.taxableAmount) > 0 else self.lineAmount

                    detail_insert_query = "insert into " + db2_library + ".F564206D (SD55SRCSYS, SD55SRDOCO," \
                                                                         " SD55SRLNID, SDEDSP, SDKCOO, SDDOCO," \
                                                                         " SDDCTO, SDLNID, SDMCU, SDCO, SDAN8," \
                                                                         " SDDRQJ, SDLITM, SDUORG, SDLPRC, SDUPC3," \
                                                                         " SDCARS, SDPSN, SDAEXP, SDUOM, SDATXA," \
                                                                         " SDATXN, SDSTAM, SDTXR1, SDURRF, SDURAB," \
                                                                         " SDURDT, SDURCD, SDURAT, SDUSER, SDUPMJ," \
                                                                         " SDTDAY, SDJOBN, SDPID, SDUPRC, SD56DPRC)" \
                                                                         " values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," \
                                                                         " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," \
                                                                         " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
                    detail_insert_query_values = (self.sourceSystem, self.sourceOrderId, int(self.sourceOrderItemId),
                                                  self.process_flag, self.orderCompany, int(self.jdeOrderId), self.orderType,
                                                  int(self.lineId), self.detailBusinessUnit, self.orderDetailCompany,
                                                  int(self.billingAddressId), int(self.promisedDate), self.sku, int(self.quantity),
                                                  int(self.suggestedRetailPrice), is_edi_item, int(self.detailCarrier),
                                                  int(self.pickSlipNumber), int(self.extendedPrice), self.unitOfMeasure,
                                                  self.taxableAmount, self.taxExempt, self.tax, self.taxRate, '', 0, 0,
                                                  '', 0, '', int(current_jl_date_Date), currentTime, '', program_id,
                                                  self.unitPrice, self.discount)

                    #print(detail_insert_query)
                    logging.info("Py_Integrator: Insert query for details table F564206D: " + str(detail_insert_query)
                                 + " and insert query values are: ")
                    logging.info(detail_insert_query_values)
                    logging.info("Py_Integrator: Executing insert query for details table F564206D.")
                    cursor.execute(detail_insert_query, detail_insert_query_values)
                    cursor.commit()
            logging.info("Insert into DB2 Details table F564206D success......")
            logging.info("Py_Integrator: Inserted the header message successfully into DB2 detail table F564206D.")
            cursor.close()
            conn.close()

            self.sourceCustomerId               = str(message_record.get("sourceCustomerId", 0))
            if message_record.get("orderDate"):
                order_date_obj = datetime.strptime(message_record.get("orderDate"), '%m-%d-%Y')
                self.orderDate = self.pydate_to_julian_date(order_date_obj.date())
            else:
                self.orderDate = ""
            self.accountName                    = self.process_special_char(message_record.get("accountName", "")[0:40])
            self.billingAddress1                = self.process_special_char(message_record.get("billingAddress").get("address1", "")[0:40])
            self.billingAddress2                = self.process_special_char(message_record.get("billingAddress").get("address2", "")[0:40])
            self.billingAddress3                = self.process_special_char(message_record.get("billingAddress").get("address3", "")[0:40])
            self.billingCity                    = self.process_special_char(message_record.get("billingAddress").get("city", "")[0:25])
            self.billingState                   = message_record.get("billingAddress").get("state", "")
            self.billingPostalCode              = message_record.get("billingAddress").get("postalCode", "")
            self.billingCountry                 = message_record.get("billingAddress").get("country", "")
            self.billingAddressType             = message_record.get("billingAddress").get("addressType", "")
            self.billingCompany                 = self.process_special_char(message_record.get("billingAddress").get("company", ""))
            self.billingTelephone               = message_record.get("billingAddress").get("telephone", "")[0:20]
            self.billingFirstName               = self.process_special_char(message_record.get("billingAddress").get("firstName", ""))[0:25]
            self.billingLastName                = self.process_special_char(message_record.get("billingAddress").get("lastName", "")[0:25])
            self.shippingAddressId              = str(message_record.get("shippingAddress").get("addressId", 0))
            self.shippingAddress1               = self.process_special_char(message_record.get("shippingAddress").get("address1", "")[0:40])
            self.shippingAddress2               = self.process_special_char(message_record.get("shippingAddress").get("address2", "")[0:40])
            self.shippingAddress3               = self.process_special_char(message_record.get("shippingAddress").get("address3", "")[0:40])
            self.shippingCity                   = self.process_special_char(message_record.get("shippingAddress").get("city", ""))[0:25]
            self.shippingState                  = message_record.get("shippingAddress").get("state", "")
            self.shippingPostalCode             = message_record.get("shippingAddress").get("postalCode", "")
            self.shippingCountry                = message_record.get("shippingAddress").get("country", "")
            self.shippingAddressType            = message_record.get("shippingAddress").get("addressType", "")
            self.shippingTelephone              = message_record.get("shippingAddress").get("telephone", "")[0:20]
            self.shippingFirstName              = self.process_special_char(message_record.get("shippingAddress").get("firstName", "")[0:25])
            self.shippingLastName               = self.process_special_char(message_record.get("shippingAddress").get("lastName", "")[0:25])
            self.carrier                        = str(message_record.get("shippingAddress").get("carrier", 0))
            self.freightHandling                = ((message_record.get("shippingAddress").get("freightHandling", "")).split(','))[0]
            self.deliveryInstructions           = self.process_special_char(message_record.get("shippingAddress").get("deliveryInstructions", "")[0:30])
            self.deliveryInstructions2          = self.process_special_char(message_record.get("shippingAddress").get("deliveryInstructions2", "")[0:30])
            self.purchaseOrderNumber            = self.process_special_char(message_record.get("purchaseOrderNumber", "")[0:25])
            self.currency                       = message_record.get("currency", "")
            self.paymentTerms                   = message_record.get("paymentTerms", "")
            self.orderTakenBy                   = self.process_special_char(str(message_record.get("orderTakenBy", 0)[0:10]))
            self.orderedBy                      = self.process_special_char(message_record.get("orderedBy", "")[0:10])

            self.lineOfBusiness                 = message_record.get("lineOfBusiness", "")
            self.authorizationNumber            = message_record.get("authorizationNumber", "")
            self.commissionCodeRep              = message_record.get("commissionCodeRep", "")
            self.commissionCodeIRep             = message_record.get("commissionCodeIRep", "")
            self.commissionCodeDescriptionRep   = message_record.get("commissionCodeDescriptionRep", "")
            self.hasEDIOrderLines               = str(message_record.get("hasEDIOrderLines", ""))
            self.lastTransactionId              = str(message_record.get("lastTransactionId", 0))
            self.pickSlipNumber                 = str(message_record.get("pickSlipNumber", 0))
            self.ediBatchNumber                 = str(message_record.get("ediBatchNumber", 0))
            self.orderTotal                     = str(format(message_record.get("orderTotal", 0.0) * 100, '.0f'))
            self.totalTaxable                   = str(message_record.get("totalTaxable", 0) * 100)
            self.totaltaxExempt                 = str(format(message_record.get("totaltaxExempt", 0) * 100, '.0f'))
            self.totalTax                       = str(format(message_record.get("totalTax", 0) * 100, '.0f'))

            header_insert_query =  "insert into " + db2_library + ".F564206H (SH55SRCSYS, SH55SRDOCO, SHEDSP, SHKCOO," \
                                                                  " SHDOCO, SHDCTO, SHMCU, SHCO, SHAN8, SHALPH," \
                                                                  " SHTRDJ, SHVR01, SHDEL1, SHDEL2, SHFRTH, SHCARS," \
                                                                  " SHCRCD, SHLOB, SHPTC, SHCACT, SHAUTN, SHRCD," \
                                                                  " SHTKBY, SHORBY, SHFNAME, SHLNAME, SHADD1, SHADD2," \
                                                                  " SHADD3, SHADDZ, SHCTY1, SHADDS, SHCTR, SHPH1," \
                                                                  " SHSHAN, SH55FNAME, SH55LNAME, SHADDRLIN1," \
                                                                  " SHADDRLIN2, SHADDRLIN3, SH55ADDZ, SH55CTY1," \
                                                                  " SH55ADDS, SH55CTR, SHPH2, SHAEXP, SHATXA, SHATXN," \
                                                                  " SHSTAM, SHURRF, SHURAB, SHURDT, SHURCD, SHURAT," \
                                                                  " SHUSER, SHUPMJ, SHTDAY, SHJOBN, SHPID, SHEDBT)" \
                                                                  " values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," \
                                                                  " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," \
                                                                  " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?," \
                                                                  " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
            header_insert_query_values = (self.sourceSystem, int(self.sourceOrderId), self.process_flag,
                                          self.orderCompany, int(self.jdeOrderId), self.orderType, self.businessUnit,
                                          self.orderDetailCompany, int(self.billingAddressId), self.accountName,
                                          int(self.orderDate), self.purchaseOrderNumber, self.deliveryInstructions,
                                          self.deliveryInstructions2, self.freightHandling, int(self.carrier), self.currency,
                                          self.lineOfBusiness, self.paymentTerms, self.lastTransactionId,
                                          self.authorizationNumber, '', self.orderTakenBy, self.orderedBy,
                                          self.billingFirstName, self.billingLastName, self.billingAddress1,
                                          self.billingAddress2, self.billingAddress3, self.billingPostalCode,
                                          self.billingCity, self.billingState, self.billingCountry,
                                          self.billingTelephone, self.shippingAddressId, self.shippingFirstName,
                                          self.shippingLastName, self.shippingAddress1, self.shippingAddress2,
                                          self.shippingAddress3, self.shippingPostalCode, self.shippingCity,
                                          self.shippingState, self.shippingCountry, self.shippingTelephone,
                                          int(self.orderTotal), int(self.totalTaxable), int(self.totaltaxExempt), int(self.totalTax), '', 0,
                                          0, '', 0, '', int(current_jl_date_Date), int(currentTime), '', program_id, 0)
            logger.info("Py_Integrator: The headers insert query for table F564206H: " + str(header_insert_query)
                        + " and headers insert query values are: ")
            logger.info(header_insert_query_values)
            conn = DB2Connection.get_db2_connection()
            cursor = conn.cursor()
            logger.info("Py_Integrator: Executing headers insert query for table F564206H.")
            cursor.execute(header_insert_query, header_insert_query_values)
            cursor.commit()
            logger.debug("Py_Integrator: Inserted the header message successfully into DB2 Header table F564206H.")
            cursor.close()
            conn.close()
        except Exception as e:
            print("Exception caught, error message: ", e)
            logger.info("Py_Integrator: Caught an exception in create order in insert_into_db2_order_staging_table(), error: " + str(e))

    def is_order_create_set_on(self):
        try:
            logger.info("Py_Integrator: Inside is_order_create_set_on function")
            order_set_query = "select COFLAG from CRPDTA.F5642CON where CO55CTYPE='ORDERSADD'"
            conn = DB2Connection.get_db2_connection()
            cursor = conn.cursor()
            logger.info("Py_Integrator: Executing the query: " + str(order_set_query))
            cursor.execute(order_set_query)
            rows = cursor.fetchone()
            return rows[0]
        except Exception as e:
            logger.info("Py_Integrator: Caught an exception in create order in is_order_create_set_on(), error: " + str(e))

    def process_order(self):
        while(True):
            try:
                credentials = pika.PlainCredentials(rabbitmq_username, decrypt_password(rabbitmq_password))
                parameters = pika.ConnectionParameters(rabbitmq_host, rabbitmq_port, rabbitmq_vhost, credentials)
                connection = pika.BlockingConnection(parameters)
                channel = connection.channel()
                channel.basic_consume(rabbitmq_order_queue, self.get_order_record_message)
                try:
                    channel.start_consuming()
                except KeyboardInterrupt:
                    channel.stop_consuming()
                    logging.error("Py_Integrator: Consuming interrupted.")
                connection.close()
                break
            except Exception as e:
                logger.info("Py_Integrator: Caught an exception in rabbitmq consumer. Retrying" + str(e))
                continue
            except pika.exceptions.AMQPConnectionError:
                print("Py_Integrator: Connection was closed, retrying...")
                continue
                #self.insert_to_order_DL_queue(self.payload)