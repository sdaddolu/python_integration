from py_integration.processOrders.invoice import Invoice
from py_integration.processOrders.shipment import Shipment
from py_integration.processOrders.create_account import AccountsCreate
from py_integration.processOrders.accountUpdate import AccountUpdate
from py_integration.processOrders.create_order import CreateOrder
from py_integration.processOrders.create_tax import CreateTax
from py_integration.processOrders.async_tax import AsyncTax
from py_integration.configurations.environment_properties import *

logger = logging.getLogger(__name__)

class ProcessOrder(object):
    def __init__(self, call_type, ukid):
        self.call_type = call_type
        self.ukid = ukid

    def process_order(self):
        calltype = "TAXGET"
        if self.call_type in ('TAXGET', 'TAXGETP', 'TAXGETAR', 'TAXGETR', 'TAXPOST', 'TAXPOSTAR', 'TAXCOMMIT', 'TAXGETPS'):
            calltype = self.call_type
        order_types = {"INVOICE": Invoice, "SHIPMENT": Shipment, "ACCOUNTADD": AccountsCreate,
                       "ACCOUNTUP": AccountUpdate, "ORDERSADD": CreateOrder, calltype: CreateTax, "TAXASYNC": AsyncTax}
        order_obj = order_types.get(self.call_type)(self.call_type, self.ukid)
        order_obj.process_order()
        logger.debug("Py_Integrator: Mapped object's process order called based on call type" + self.call_type +
                      " receiving from JDE ")
