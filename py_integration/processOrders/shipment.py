import py_integration.processOrders.db_access as dba
import py_integration.configurations.environment_properties as prop
from collections import defaultdict
import py_integration.processOrders.insert_rabbitMQ as rb
from datetime import datetime


logger = prop.logging.getLogger(__name__)

class Shipment(object):
    def __init__(self, call_type, ukid):
        self.ukid = ukid
        self.call_type = call_type
        # self.invoiceType = ""
        # self.invoiceCompany = ""
        self.sourceSystem = ""
        self.sourceSystemOrderId = ""
        self.invoiceId = ""
        self.e1OrderId = ""
        self.e1OrderIdCount = ""
        # self.e1OrdLine = ""
        self.sku = ""
        self.quantity = ""
        self.itemAmount = ""
        self.ordNextStatus = ""
        self.sourceSystem = ""
        self.itemId = ""
        self.status = ""
        self.message_status = ""
        self.invoiceNum = ""
        self.items = []
        # self.trackingNo = ""
        # self.carrierCode = ""
        self.rep_code = ""
        self.rep_code_description = ""
        self.irep_code = ""
        self.irep_code_description = ""
        self.purchaseOrderNumber = ""
        self.e1LineId = ""
        self.UOM = ""
        self.carrier = ""
        self.trackingNumber = ""
        self.lastStatus = ""
        self.nextStatus = ""
        self.lastStatusDescription = ""
        self.nextStatusDescription = ""
        self.additionalInfo = ""
        self.promisedDate = ""
        self.shipDate = ""
        # self.invoiceDate = ""
        self.orderType = ""
        self.customerNumber = ""
        self.invoiceStatus = ""
        self.orderDate = ""
        self.itemName = ""
        self.data = None

    def julian_to_date(self, j_date):
        if j_date:
            date_obj = datetime.strptime(j_date, '%y%j').date()
            standard_date = date_obj.strftime("%m/%d/%Y")
            return standard_date
        else:
            return ""

    def process_order(self):
        db_query = "select xr55srcsys as sourceSystem, xr55srdoco as sourceSystemOrderId, sddoco as e1OrderId, " \
                   "SDAN8 as customerNumber, M2SLSM as rep_code, M2ALPH as rep_code_description, M2SLSP as irep_code," \
                   "M2ALPH2 as irep_code_description, M2VR01 as purchaseOrderNumber, SDDCTO as orderType, " \
                   "SDTRDJ as orderDate, SDLNID/1000 as e1LineId, xr55srlnid as itemId, SDDSC1 as itemName, " \
                   "sdlitm as sku, sdsoqs/10000 as quantity, SDUOM as UOM, sdaexp/100 as itemAmount , SDOPDJ as promisedDate, " \
                   "SDADDJ as shipDate, M2DL02 as carrier, M256CSTRA as trackingNumber, M2LTTR as lastStatus, " \
                   "M2STDS as lastStatusDescription, M2NXTR as nextStatus, M2LNDS as nextStatusDescription, " \
                   "SDSRP4 as additionalInfo, SDUPRC/10000 as unitPrice, XR55SROOID as internalSourceOrderId from " + prop.db2_library + ".F5542008 inner join " \
                   + prop.db2_library + ".F4211 on m2doco = sddoco and m2litm = sdlitm " \
                   "and m2nxtr = sdnxtr left outer join " + prop.db2_library + ".F5642XEF on xrdoco = sddoco " \
                    "and xrlitm = sdlitm where m2ukid = " + self.ukid + " and sdnxtr = '578' and m2#eo = 'P' " \
                    "order by sddoco, M256CSTRA"
        self.data = dba.ReadDataFromDB2.read_data_from_ibm_db2(db_query)
        if self.data:
            logger.debug("Py_Integrator: able to read from db")
        else:
            logger.error("Py_Integrator: No data received from db, Exiting...")
            exit(0)

        payload_dict = defaultdict(list)
        payload_data = {
                "sourceSystem": "",
                "sourceSystemOrderId": "",
                "e1OrderId": 0,
                "customerNumber": 0,
                "rep_code": 0,
                "rep_code_description": "",
                "irep_code": 0,
                "irep_code_description": "",
                "purchaseOrderNumber": "",
                "orderType": "",
                "orderDate": 0,
                "sourceSystemInternalOrderId": "",
                "items": []
        }
        l = set()
        for r in self.data:
            l.add(str(r[2]))
        l = list(l)
        for inv in l:
            for row in self.data:
                if inv == str(row[2]):
                    payload_dict[inv].append(row)
        for k, v in payload_dict.items():
            for item in v:
                self.sourceSystem = "" if not item[0] else item[0]
                self.sourceSystemOrderId = "" if not item[1] else item[1]
                self.e1OrderId = 0 if not item[2] else item[2]
                self.customerNumber = 0 if not item[3] else item[3]
                self.rep_code = 0 if not item[4] else item[4]
                self.rep_code_description = "" if not item[5] else item[5]
                self.irep_code = 0 if not item[6] else item[6]
                self.irep_code_description = "" if not item[7] else item[7]
                self.purchaseOrderNumber = "" if not item[8] else item[8]
                self.orderType = "" if not item[9] else item[9]
                self.orderDate = 0 if not item[10] else item[10]
                self.e1LineId = 0.0 if not item[11] else float(item[11])
                self.itemId = "" if not item[12] else item[12]
                self.itemName = "" if not item[13] else item[13]
                self.sku = "" if not item[14] else item[14]
                self.quantity = 0 if not item[15] else item[15]
                self.UOM = "" if not item[16] else item[16]
                self.itemAmount = 0 if not item[17] else item[17]
                self.promisedDate = 0 if not item[18] else item[18]
                self.shipDate = 0 if not item[19] else item[19]
                self.carrier = 0 if not item[20] else item[20]
                self.trackingNumber = "" if not item[21] else item[21]
                self.lastStatus = "" if not item[22] else item[22]
                self.lastStatusDescription = "" if not item[23] else item[23]
                self.nextStatus = "" if not item[24] else item[24]
                self.nextStatusDescription = "" if not item[25] else item[25]
                self.additionalInfo = "" if not item[26] else item[26]
                self.unitPrice = 0 if not item[27] else item[27]
                self.sourceSystemInternalOrderId = "" if not item[28] else item[28]

                self.promisedDate = self.julian_to_date(str(int(self.promisedDate))[1:])
                self.shipDate = self.julian_to_date(str(int(self.shipDate))[1:])
                # self.invoiceDate = julian_to_date(str(int(self.invoiceDate))[1:])
                self.orderDate = self.julian_to_date(str(int(self.orderDate))[1:])

                item_dict = {
                    "e1LineId": int(self.e1LineId),
                    "itemId": str(self.itemId).strip() if str(self.itemId) else str(self.itemId),
                    "itemName": str(self.itemName).strip() if str(self.itemName) else str(self.itemName),
                    "sku": self.sku.strip() if self.sku else self.sku,
                    "quantity": float(self.quantity),
                    "UOM": self.UOM,
                    "itemAmount": float(self.itemAmount),
                    "promisedDate": self.promisedDate,
                    "shipDate": self.shipDate,
                    # "invoiceDate": self.invoiceDate,
                    "carrier": self.carrier.strip() if self.carrier else self.carrier,
                    "trackingNumber": self.trackingNumber.strip() if self.trackingNumber.strip() else "NOT AVAILABLE",
                    "lastStatus": self.lastStatus.strip() if self.lastStatus else self.lastStatus,
                    "lastStatusDescription": self.lastStatusDescription.strip() if self.lastStatusDescription
                    else self.lastStatusDescription,
                    "nextStatus": self.nextStatus.strip() if self.nextStatus else self.nextStatus,
                    "nextStatusDescription": self.nextStatusDescription.strip() if self.nextStatusDescription else
                    self.nextStatusDescription,
                    "additionalInfo": self.additionalInfo,
                    "unitPrice": float(self.unitPrice)
                }

                s = str(int(self.e1OrderId))
                payload_data["sourceSystem"] = self.sourceSystem.strip()
                payload_data["sourceSystemOrderId"] = str(self.sourceSystemOrderId.strip())
                payload_data["e1OrderId"] = int(s)
                payload_data["customerNumber"] = int(self.customerNumber)
                payload_data["rep_code"] = str(self.rep_code)
                payload_data["rep_code_description"] = self.rep_code_description.strip()
                payload_data["irep_code"] = str(self.irep_code)
                payload_data["irep_code_description"] = self.irep_code_description.strip()
                payload_data["purchaseOrderNumber"] = str(self.purchaseOrderNumber).strip()
                payload_data["orderType"] = self.orderType
                payload_data["orderDate"] = self.orderDate
                payload_data["sourceSystemInternalOrderId"] = str(self.sourceSystemInternalOrderId).strip()
                payload_data["items"].append(item_dict)
            logger.debug("Py_Integrator: Writing to RabbitMQ, customerNumber: " + str(payload_data["customerNumber"]) +
                         "\n")
            rb.InsertToRabbitMQ.insert_into_rabbitmq(payload_data, self.nextStatus)
            dba.UpdateDB2.update_db2(rb.InsertToRabbitMQ.msg_status, self.call_type, payload_data["e1OrderId"],
                                         self.sku.strip(), self.nextStatus, self.ukid)
            payload_data = {
                    "sourceSystem": "",
                    "sourceSystemOrderId": "",
                    "e1OrderId": 0,
                    "customerNumber": 0,
                    "rep_code": "",
                    "rep_code_description": "",
                    "irep_code": "",
                    "irep_code_description": "",
                    "purchaseOrderNumber": "",
                    "orderType": "",
                    "orderDate": 0,
                    "sourceSystemInternalOrderId": "",
                    "items": []
               }
