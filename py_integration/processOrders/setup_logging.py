import logging
import sys
from logging.handlers import RotatingFileHandler
from datetime import datetime
import py_integration.configurations.environment_properties as prop


def setup_log_file(logger_name):
    logger_name = prop.splunk_server_log_folder_path + logger_name + "_" + datetime.now().strftime('%Y-%m-%d') + ".log"
    logger = logging.getLogger(logger_name)
    logger.setLevel(prop.debug_level)
    logging.basicConfig(handlers=[RotatingFileHandler(logger_name, mode='a', maxBytes=100*1024*1024, backupCount=100)],
                        level=prop.debug_level, format=prop.log_format)



if __name__ == "__main__":
    setup_log_file('setup_log')
    log = logging.getLogger('setup_log')
    log.info("Py_Integrator: Dry run for setup_logging")
