from py_integration.util.secure_creds import *
import pyodbc as po
from py_integration.configurations.environment_properties import *

logger = logging.getLogger(__name__)



# Singleton class for database connection
class Singleton(type):
    _instances = {}

    # overriding __call__ to make class singleton
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

class DB2Connection(metaclass=Singleton):
    db2_conn = None
    password = decrypt_password(db2_password)
    db_host = db2_host
    db2_conn_str = "DRIVER=" + DRIVER + "SYSTEM=" + SYSTEM + "UID=" + db2_user + "PWD=" + password

    def __init__(self):
        pass
        #print("Connecting to IBM DB2..")

    @classmethod
    def get_db2_connection(cls):
        try:
            logger.debug("Py_Integrator: Trying IBM DB2 connection.")
            cls.db2_conn = po.connect(cls.db2_conn_str)
            logger.info("Py_Integrator: IBM DB2 connection successful.")
            return cls.db2_conn
        except po.Error as pe:
            #print("Couldn't connect to IBM db2", pe)
            logger.error("Py_Integrator: Couldn't connect to IBM db2 with exception : " + str(pe))
        except Exception as e:
            #print("There is a problem with IBM db2", e)
            logger.error("Py_Integrator: Couldn't connect to IBM db2 with exception : " + str(e))

    @classmethod
    def close_connection(cls):
        cls.db2_conn.close()
        logger.debug("Py_Integrator: Closing IBM DB2 connection.")
