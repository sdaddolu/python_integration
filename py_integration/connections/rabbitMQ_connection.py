import pika
import json
import pika.exceptions as pe
from py_integration.util.secure_creds import *
from py_integration.configurations.environment_properties import *

logger = logging.getLogger(__name__)


# Singleton class for database connection
class Singleton(type):
    _instances = {}
    # overriding __call__ to make class singleton

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


# based on the env setup creds for the rabbit and db2
class RabbitMQConnection(metaclass=Singleton):
    username = rabbitmq_username
    vhost = rabbitmq_vhost
    password = decrypt_password(rabbitmq_password)
    host = rabbitmq_host
    port = rabbitmq_port
    connection = None
    channel = None
    parameters = None
    credentials = None

    def __init__(self):
        pass
        #print("Connecting to RabbitMQ..")

    @classmethod
    def get_connection(cls):
        try:
            logger.debug("Py_Integrator: Trying RabbitMQ connection.")
            cls.credentials = pika.PlainCredentials(cls.username, cls.password)
            cls.parameters = pika.ConnectionParameters(cls.host, cls.port, cls.vhost, cls.credentials)
            cls.connection = pika.BlockingConnection(cls.parameters)
            cls.channel = cls.connection.channel()
            logger.info("Py_Integrator: RabbitMQ connection successful")
            return cls.channel
        except pe.AMQPChannelError as ch_err:
            #print("Failed to create channel with RabbitMQ connection: " + str(ch_err))
            logger.error("Py_Integrator: Failed to create channel with RabbitMQ connection: " + str(ch_err))
        except pe.AuthenticationError as auth:
            logger.error("Py_Integrator: Authentication failed for RabbitMQ, please check all the credentials "
                          "are correct: " + str(auth))
        except pe.AMQPConnectionError as con_err:
            logger.error("Py_Integrator: Couldn't connect to RabbitMQ, please check if RabbitMQ is running or may be "
                          "connection is refused: " + str(con_err))
        except Exception as e:
            logger.error("Py_Integrator: Couldn't connect to RabbitMQ with exception : " + str(e))

    @classmethod
    def close_connection(cls):
        cls.connection.close()
        logger.debug("Py_Integrator: Closing RabbitMQ connection.")
