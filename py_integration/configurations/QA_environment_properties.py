# DV Environment fields - RabbitMQ and IBM i Series DB2 creds
import logging
splunk_server_log_folder_path = r"E:\\splunk_logs\\"
debug_level = logging.INFO
log_format = '%(asctime)s | %(levelname)s | %(message)s'

# RabbitMQ creds
rabbitmq_username = "ihylgide"
rabbitmq_password = b'VFdENGFsNlRHMl9JUzVJc08ydjcxbXJGdjh0MENWMmw='
rabbitmq_host = "hippy-wombat.rmq.cloudamqp.com"
rabbitmq_port = 5672
rabbitmq_invoice_exchange = "e.order.status.invoice"
rabbitmq_invoice_routing_key = "r.order.status.invoice.general"
rabbitmq_shipment_exchange = "e.order.status.shipment"
rabbitmq_shipment_routing_key = "r.order.status.shipment.jde.out"
rabbitmq_queue = "q.order.status.invoice"
rabbitmq_account_exchange = "e.account.create"
rabbitmq_account_routing_key = "r.account.upsert.jde.in"
rabbitmq_account_queue = "q.account.upsert.jde.in"
rabbitmq_order_queue = "q.order.create.jde.in"
rabbitmq_order_exchange = "e.order.create"
rabbitmq_order_DL_exchange = "e.order.create.error"
rabbitmq_order_routing_key = "r.order.create.jde.in"
rabbitmq_order_DL_routing_key = "r.order.create.jde.in.error"
rabbitmq_order_DL_queue = "q.order.create.jde.in.error"
rabbitmq_vhost = "mule-dev"
# IBM i Series DB2 creds
DRIVER="{iSeries Access ODBC Driver};"
SYSTEM="ATJDEQA1;"
db2_user = "python;"
db2_password = b'cHl0aG9ucWEx'
db2_host = "10.20.20.63"
db2_library = "QADTA"
m2user = "python"
program_id = "python"