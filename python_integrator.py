import sys
import py_integration.processOrders.processOrder as po
from py_integration.configurations.environment_properties import *
from py_integration.processOrders.setup_logging import setup_log_file

def send_order_status_updates(call_type, ukid):
    setup_log_file(call_type)
    logging.info("Py_Integrator: Received request for call type : " + call_type + " and ukid : " + ukid + " from JDE ")
    pobj = po.ProcessOrder(call_type, ukid)
    pobj.process_order()

if __name__ == "__main__":
    n = len(sys.argv)
    if n != 3:
        #print("Must send two arguments: call_type and ukid. Exiting....")
        logging.error("Py_Integrator: Must send two arguments: call_type and ukid. Exiting....")
        exit(0)
    send_order_status_updates(sys.argv[1], sys.argv[2])
